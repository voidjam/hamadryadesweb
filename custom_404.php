<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         -->
<!-- 404 ERROR PAGE 																			 -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         -->
<!-- Apache 2 cofiguration: Include ErrorDocument directive on httpd.conf.						 -->
<!-- ###################################################################################         -->
<!-- # HAMADRYADES  ####################################################################		 -->
<!-- # This code maps the adress www.peter.es:8080 to the corresponding file Options 			 -->
<!-- # "/home/marcos/VoidJam/Proyectos/Hamadryades/hamadryadesweb".								 -->
<!-- # We must also modify hosts.conf in orther to make www.hamadryades.es local as follows:	 -->
<!-- #																							 -->
<!-- #	127.0.0.1	www.hamadryades.es 															 -->
<!-- #																							 -->
<!-- ###################################################################################		 -->
<!-- 																							 -->
<!-- <VirtualHost *:8080>																		 -->
<!--   ServerName www.<hamadryades class="es"></hamadryades>									 -->
<!--   ServerAlias hamadryades.es 																 -->
<!--   DocumentRoot /home/marcos/VoidJam/Proyectos/Hamadryades/hamadryadesweb					 -->
<!--   DirectoryIndex index.php 																 -->
<!--   AllowOverride None																		 -->
<!-- 																							 -->
<!--   ErrorDocument 404 /custom_404.php 														 -->
<!-- 																							 -->
<!-- 																							 -->
<!-- 																							 -->
<!--   <Directory /home/marcos/VoidJam/Proyectos/Hamadryades/hamadryadesweb> 					 -->
<!--     Options Indexes FollowSymLinks MultiViews												 -->
<!--     AllowOverride None																		 -->
<!--     Require all granted																	 -->
<!--   </Directory>																				 -->
<!-- 																							 -->
<!-- </VirtualHost>																				 -->
<!-- # HAMADRYADES  ####################################################################		 -->
<!-- ###################################################################################		 -->

<?php 
    // Define root path and page name.
    $rootpath = './';
    $pageTitle = 'Error 404';

    // Including headers.
    include ($rootpath.'inc/header.php');

?>
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Home Page Content -->
<div class="e404_content">


  	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- VINES -->
	<!-- Here we will put the canvas fot the vines animation -->
	<figure id="vines-figure"></figure>
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


</div>

<script type="text/javascript">
	showVinesAnimation("4o4");
</script>

<?php
  // Including footer.
  include ($rootpath.'inc/footer.php');

    // This results in an error.
    // The output above is before the header() call
    //sleep(5);
    //header('Location: http://www.hamadryades.es/home');

?>