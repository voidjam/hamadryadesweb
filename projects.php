<?php

    include ($rootpath.'inc/strings.php');

    $language = 'es';
    $strings = array(
        ///////////////////////////////////////////////////////////////////////
        // SPANISH ////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////
        'es' => array( 
           117   => '<strong>¡Hecho!</strong> Hemos enviado su email. Le contestaremos lo antes posible.',
           118   => '<strong>¡Error!</strong> Su email no ha podido ser enviado.<br>Por favor, intentelo de nuevo más tarde.'
          ),
        ///////////////////////////////////////////////////////////////////////
        // ENGLISH ////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////
        'en' => array( 
           117   => '<strong>Done!</strong> Your email has been sent. We will answer you as soon as possible.',
           118   => '<strong>Error!</strong> Your email could not be sent. Please try again later.'
          )
    );

 ?>
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- Projects Page @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<script id="projects_content" type="text/template">
    <div class="projects_content"> <!-- ALV: needed for cp class to work -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- PAGE TITTLE -->
        <div class="about_text_header projects_text_header"> <!-- ALV: needed for cp class to work -->
            <h2 id="cp"><span>Nuestros</span></h2>
            <h1 id="cp"><span>PROYECTOS</span></h1>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SERVICES LINKS -->
        <!-- The data-serv-chk attribute links each service with its project-content div -->
        <!-- and with the corresponding li elements of the dots side menu. -->

        <div class="services-links-container text-center">

            <input type="checkbox" id="serv-paisaje" data-serv-chk="paisaje">
            <label class="services-link" for="serv-paisaje">
                <img src="./images/icons/paisaje_CU.jpg" alt="Click para filtrar los proyectos según el Servicio."
                     title="Seleccionar solamente los proyectos de Estudios de Paisaje."/>
                <span>Estudios de Paisaje</span>
            </label>

            <input type="checkbox" id="serv-paisajismo" data-serv-chk="paisajismo">
            <label class="services-link" for="serv-paisajismo">
                <img src="./images/icons/paisajismo_CU.jpg" alt="Click para filtrar los proyectos según el Servicio."
                     title="Seleccionar solamente los proyectos de Paisajismo."/>
                <span>Paisajismo</span>
            </label>

            <input type="checkbox" id="serv-mma" data-serv-chk="mma">
            <label class="services-link" for="serv-mma">
                <img src="./images/icons/mma_CU.jpg" alt="Click para filtrar los proyectos según el Servicio."
                     title="Seleccionar solamente los proyectos de Medio Ambiente."/>
                <span>Medio Ambiente</span>
            </label>

            <input type="checkbox" id="serv-agro" data-serv-chk="agro">
            <label class="services-link" for="serv-agro">
                <img src="./images/icons/agro_CU.jpg" alt="Click para filtrar los proyectos según el Servicio."
                     title="Seleccionar solamente los proyectos de Agronomía y Agricultura."/>
                <span>Agronomía y Agricultura</span>
            </label>

            <input type="checkbox" id="serv-world_investigacion"  data-serv-chk="world_investigacion">
            <label class="d-none d-md-flex services-link" for="serv-world_investigacion" onClick="window.open('https://drive.google.com/open?id=1lml24xkZ6RqtPfITFyXz7-iPemk&usp=sharing','_blank')">
                <img src="./images/icons/world_investigacion_CU.jpg" alt="Click para filtrar los proyectos según el Servicio."
                     title="Os facilitamos a través de google maps la ubicación y descripción de la totalidad de los proyectos realizados hasta la fecha."/>
                <span>Todos los Proyectos</span>
            </label>

        </div>
        <!-- <small>**Puede filtrar los proyectos pinchando en el servicio correspondiente.</small> -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->






        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- PROJECTS GRID -->
        <div class="grid grid-projects">

            <?php

            foreach (array_values($projects) as $key => $proj)  {

                $data_index = str_pad($key, 2, '0', STR_PAD_LEFT);

                echo '<!-- PROYECT '.$key.'-->';
                echo '<div data-index="'.$data_index.'" data-serv="'.$proj["project_serv"].'" class="project '.((($key % 2) == 1) ? 'project--odd' : 'project--even').' column span-8-10 gutter-l-1-10 gutter-r-1-10 span-s-10-10">';
                echo '<div class=\'project-content\'>';
                    echo '<!-- FluidBox Link -->';
                    //echo '<a class=\'project-image\' data-fluidbox href=\''.$rootpath.$proj["project_image"].'\' title=\'\'>';
                    echo '<a class=\'project-image\' data-fancybox=\'gallery'.$key.'\' href=\''.$rootpath.$proj["project_image"].'\' title=\'\'>';
                    echo '<img src="'.$rootpath.$proj["project_image"].'" alt="'.$proj["project_alt"].'" title="'.$proj["project_hover"].'">';
                    echo '</a>';
                    echo '<a data-fancybox=\'gallery'.$key.'\' class=\'img-fluid\' href=\''.$rootpath.$proj["project_image_2_opt"].'\' title=\'\'></a>';
                    echo '<!-- Project INFO Link -->';
                    echo '<div class="project-link" data-index="'.$data_index.'">';
                        echo '<!-- CLOSE BUTTON -->';
                        echo '<input class="close_button" id="close_button_'.$data_index.'" type="checkbox"/>';
                        echo '<!-- Sub Header -->';
                        echo '<span class="project-link-title">'.$proj["project_title"].'</span>';
                        echo '<span class="project-link-addinfo-location d-md-none">'.$proj["project_addinfo_location"].'</span>';
                        echo '<!-- Header -->';
                        echo '<h2 class="project-link-description">'.$proj["project_description"].'</h2>';
                        echo '<!-- Additional Info -->';
                        echo '<div class="project-link-addinfo-text">';
                            echo '<div class="project-link-addinfo-text-label">'.$proj["project_addinfo_text"].'</div>';
                            echo '<div class="project-link-addinfo-text-form w-100"></div>';
                        echo '</div>';
                        echo '<span class="project-link-addinfo-location d-none d-md-block">'.$proj["project_addinfo_location"].'</span>';

                        echo '<!-- Arrow Icon -->';
                        echo '<div class="project-link-arrow d-none d-md-block">'; // hidden on mobile
                            echo '<div class=\'animated-arrow\'>';
                                echo '<span class=\'the-arrow -left d-none d-md-inline\'><span class=\'shaft\'></span></span>';
                                echo '<span class=\'main\'>';
                                echo '<span class=\'text\'>Más Info</span>';
                                echo '<span class=\'the-arrow -right d-none d-md-inline\'><span class=\'shaft\'></span></span>';
                            echo '</div>';
                        echo '</div><!-- /project-link-arrow -->';
                    echo '</div><!-- /project-link -->';
                echo '</div><!-- /project-content -->';
                echo '</div><!-- /PROYECT '.$key.' -->';

            }
            ?>

            <!-- DONDE, only mobile -->
            <div class="container d-md-none mb-3">
                    <div class="row align-items-center" onClick="window.open('https://drive.google.com/open?id=1lml24xkZ6RqtPfITFyXz7-iPemk&usp=sharing','_blank')">
                        <div class="col-4 offset-4">
                            <img src="./images/icons/world_investigacion_CU.jpg"
                                 class="rounded-circle img-fluid" alt="Click para filtrar los proyectos según el Servicio."
                                 title="Os facilitamos a través de google maps la ubicación y descripción de la totalidad de los proyectos realizados hasta la fecha."
                                 style="border: 4px solid #fff; box-shadow: 0 0 4px rgba(34, 25, 25, 0.6);
                                        -moz-box-shadow: 0 0 4px rgba(34, 25, 25, 0.6);
                                        -webkit-box-shadow: 0 0 4px rgba(34, 25, 25, 0.6);"/>
                        </div><br>
                        <div class="col-12 text-center p-0">Todos los Proyectos</div>
                    </div>
            </div>

            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- FOOTER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        </div><!-- /.grid_projects -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- COPYRIGHT INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="d-lg-none copyright-mob-projects text-center">
            <span>©hamadryades 2017</span><span> | </span><span>info@hmap.es</span>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- FOOTER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="footer">
            <div class="text-white m-4" style="bottom: 0; right: 0; position: absolute;">©hamadryades 2017 | info@hmap.es</div>
            <img src="./images/footer_bn.png"/>
        </div>
        <!-- /FOOTER ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    </div> <!-- /projects_content -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- LOGO -->
    <div class="projects-logo" onclick="showPage(page_section.home)">
        <!-- Include SVG logo code -->
        <div class="projects-logo-svg">
            <?php include($rootpath.'images/start_logo.svg'); ?>
        </div>
        <span>hamadryades</span>
    </div><!-- /projects-logo -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SIDE DOTS MENU ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- The data-id attribute of the a element links each menu dot with the -->
    <!-- related project -->
    <!-- The data-serv attribute of the li element links the menu items with -->
    <!-- the corresponding service. -->
    <div class="section-indicator">
        <ul class="section-indicator-list">

            <?php
            foreach (array_values($projects) as $key => $proj) {

                echo '<li class="list-item list-item-section-indicator" data-serv="'.$proj["project_serv"].'">';
                echo '<a class="button section-indicator-button" data-id="'.str_pad($key, 2, '0', STR_PAD_LEFT).'">';
                echo '<span class="dot section-indicator-dot section-indicator-dot-normal"></span>';
                echo '<span class="dot section-indicator-dot section-indicator-dot-pulse"></span>';
                echo '<span class="label section-indicator-label">'.$proj["project_description"].'</span>';
                echo '</a>';
                echo '</li>';
            }
            ?>

        </ul>
    </div>
    <!-- /SIDE DOTS MENU +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span>PROYECTOS</span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <script id="projects_content_js" type="text/javascript">

        // AJAX: SENDING EMAIL
        function addFormListener() {
            $("#contactForm").on('submit', function(e) {
              e.preventDefault();
              navbar_height = $('.navbar').outerHeight();

                $('#formSubmit').attr("disabled", true);
                $.ajax({
                    type: "POST",
                    url: "<?php echo $rootpath; ?>libs/email-sender.php",
                    //url: "http://132.148.91.154/~hamadryades/libs/email-sender.php",
                    data: $(this).serialize(),
                    success: function(response){
                      if (response != 'success') {
                        // WARNING
                        // we scroll to top
                        $('#formSubmit').attr("disabled", false);
                        $('.project-link-addinfo-text').animate({
                            scrollTop: 0
                        });
                        $("#formResponseProjects").html(response);
                        $("#formResponseProjects").fadeOut(0,function(){
                             $(this).html(response).fadeIn();
                        });
                      } else {
                        // SUCCESS
                        // we scroll to top
                        $('.project-link-addinfo-text').animate({
                            scrollTop: 0
                        });
                        $("#formResponseProjects").html('<div class="alert alert-success alert-dismissible form-notification fade show" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                '<?php echo $strings[$language][117]; ?></div>'+
                                '<script>closeAlert();<\/script>'
                                );
                        $('#formSubmit').attr("disabled", false);
                      }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // ERROR
                        // we scroll to top
                        $('.project-link-addinfo-text').animate({
                            scrollTop: 0
                        });
                        $("#formResponseProjects").html('<div class="alert alert-danger alert-dismissible form-notification fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $strings[$language][118]; ?></div>'+
                          '<script>closeAlert();<\/script>'
                        );
                        $('#formSubmit').attr("disabled", false);
                        console.log(textStatus, errorThrown);
                      }
                });
                // Auto close form alerts
                window.setTimeout(function () {
                    $(".alert").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 5000);

            });
        }

        // AUTOSIZE TEXTAREA ON SHOW
        function autoSizeOnShow() {
            var ta = document.querySelector('textarea');
            ta.style.display = 'none';
            autosize(ta);
            ta.style.display = '';

            // Call the update method to recalculate the size:
            autosize.update(ta);

            // var ta = document.querySelector('textarea');
            // ta.addEventListener('focus', function(){
            //   autosize(ta);
            // });
        }

        function closeAlert() {
            $( ".close" ).click(function(e) {
                e.stopPropagation();
                $(".form-notification").alert('close');
            });
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////
        $(function() {

            // FANCYBOX
            $("[data-fancybox]").fancybox({
                toolbar : false
            });

            // Get all the services check buttons.
            let $check_buttons = $(".projects_content .services-links-container input[type=checkbox]");
            // We remove the last icons since it shows the projects map on google.
            $check_buttons.last().remove();

            /*
             * This function shows the first project-content div on the list when they are hidden.
             */
            function showFirstProjectOnList(){
                $(".project:not([style*='display:none']):not([style*='display: none'])").first().find(".project-content").children().addClass("fade-in-bottom");
            }
            showFirstProjectOnList();

            // Each time a service check button changes its status, we go throw
            // the 5 of them to be sure that only the corresponding projects are
            // being shown.
            // The data-serv-chk attribute of each check button has the value of the related service.
            // The data-serv attribute of the project-content divs and the li elements of the side dots menu
            // indicates what of the services they are related to, so we just have to select all elements with
            // attribute data-serv and show or hide them depending on the check button value.
            $check_buttons.change(function() {

                let $clickedBox = $(this);

                // First we hide every project:
                $(".project-content").children().removeClass("fade-in-bottom");

                // If no radio button selected show all projects.
                if ($(".projects_content .services-links-container input[type=checkbox]:checked").length == 0)  {
                    $("[data-serv]").show();
                    showFirstProjectOnList();
                // If at least 1 check button is selected we will show only the corresponding projects.
                } else {

                    // Evolution: Single Choice
                    $check_buttons.prop( "checked", false );
                    $clickedBox.prop( "checked", true );

                    $check_buttons.each(function(index, value)  {
                        // We get the service related to the check button.
                        let service = $(this).attr("data-serv-chk");
                        // We show or hide the project-content div and the dots side menu li element depending
                        // on the value of the check box button.
                        this.checked
                            ? ($(".project[data-serv="+service+"]").fadeIn(200),
                                $(".list-item[data-serv="+service+"]").show(200))
                            : ($(".project[data-serv="+service+"]").fadeOut(200, function(){
                                $(this).find(".project-content").children().removeClass("fade-in-bottom")
                            }),
                                $(".list-item[data-serv="+service+"]").hide(200));

                        if (index == ($check_buttons.length -1))    {
                            setTimeout(function(){
                                showFirstProjectOnList();
                            }, 250);
                        }
                    });
                }

                // After changes we set the active button on the side dots menu.
                setActiveButton();
            });


            /////////////////////////////////////////////////////////////////////////////
            // PROJECT LINK CLICKS //////////////////////////////////////////////////////
            /*
             * Increases project-link element's size and show the corresponding
             * info. Manages the event listeners for the project-link element to
             * be closed.
             */
            function showProject(e)  {

                // This prevents the click event from changing the checkbox button
                // state automatically, since we are clicking its parent div. We will set checked = true
                // programmatically.
                e.stopPropagation();

                if (breakpoint == 'xs' || breakpoint == 'sm') { // in mobile we close other possible open projects
                    $(".close_button:checked").trigger("click");
                }

                // Clicked .project-link div and its data-index.
                let $this = this;
                let data_index = $($this).attr("data-index");

                // Prevent scrolling the page when scrolling inside .project-link div, only
                // scrolling outside will trigger the $(window).scroll(onScroll) event.
                if (breakpoint != 'xs' && breakpoint != 'sm') {
                    $($this).on('mousewheel DOMMouseScroll', function (e) {
                        e.preventDefault();
                    });
                }

                // Deactivate onScroll listener not to be triggered by the
                // animated scroll created by showProject() function.
                $(window).off("scroll", onScroll);

                // Deactivate .project-link click listener.
                $($this).off("click", showProject);

                // Now we will increase the width and height of the project-link element on a
                // 800m css transition.
                $($this).addClass("project-link-open");
                // Show closing button and style all the elements changed by .close_button::checked property.
                $("#close_button_" + data_index).prop( "checked", true );

                // Here we manage the scrolling: we scroll the page to center vertically the
                // project-link on screen. Finally, we reactivate the window scroll listener to close
                // any open project or image when user scrolls. We use a delay because if not
                // the project-link is closed by its own animated scroll.
                setTimeout(function () {

                    // With var run we make sure that the code for animate's complete callback
                    // is just executed once.
                    let run = true;

                    let scroll_project_open_pos;

                    if (breakpoint != 'xs' && breakpoint != 'sm') {
                        scroll_project_open_pos = ( $(window).height() - $($this).outerHeight(true) ) / 2;
                    } else {
                        scroll_project_open_pos = 100;
                    }

                    // This animate is called twice, once for html and one for body.
                    $('html,body').animate({
                        scrollTop: $($this).offset().top - scroll_project_open_pos
                    }, 300, "easeOutBack", function (e) {
                        if (run)    {

                                // Show the project-link-addinfo-text div.
                                $($this).find(".project-link-addinfo-text").addClass("fade-in-bottom");
                                // Show the Contact Button:
                                setContactButton($this);
                                // Activate onClick on close_button hideProject() for the current .project-link element.
                                $($this).find(".close_button").on("click", hideProject);

                                // Close project onClick outside the project-link div.
                                if (breakpoint != 'xs' && breakpoint != 'sm') {
                                    closeOnClickOut();
                                }

                                setTimeout(function () {
                                    $(window).on("scroll", onScroll);
                                }, 150);

                                run = false;

                        }
                    });
                }, 600);

            } // End of showProject()

            /*
             * Return project-link to its original size after click on
             * close_button.
             */
            function hideProject(e)  {

                // Avoid the click to propagate to the .project-link listener.
                e.stopPropagation();
                // Getting the corresponding .project-link div.
                $project_div = $(this).parent();

                // Show More Info button and hide the close_button and the contact button:
                TweenMax.to($('.contact_button_container'), 0.2, {autoAlpha: 0, onComplete: function () {
                    $('.contact_button_container').remove();
                }});
                $project_div.remove('.contact_button_container');
                //$project_div.remove('.project-link-addinfo-contact');

                // Deactivating the listeners so the scrolling inside -project-link div provoques
                // the scrolling event to propagate.
                $project_div.off( 'mousewheel DOMMouseScroll');

                $(this).prop( "checked", false );
                $project_div.find(".project-link-addinfo-text").removeClass("fade-in-bottom");
                $project_div.removeClass("project-link-open");
                $(this).off("click", hideProject);
                $project_div.on("click", showProject);

                // We show project text just in case we hide it to show the contact form
                $project_div.find(".project-link-addinfo-text-label").show();
                // We reset the contact form div just in case we opened it
                $project_div.find(".project-link-addinfo-text-form").html('');

            } // End of hideProject()

            function closeOnClickOut()    {

                // CLOSE PROJECT-LINK ON CLICK OUTSIDE PROJECT LINK DIV.
                // Add a click listener with a custom namespace to the $window element to detect
                // clicks outside the project-link and close it.
                var $box = $(".project-link-open");
                // custom_events.clickOutsideProjectLink = "click.close_project" defines the close_project
                // namespace for this particular click event.
                // This event could be removed by using .off("click.mySomething"),
                // without removing other click handlers attached to the specified element.
                $(window).on(custom_events.clickOutsideProjectLink, function(event){
                    if ( $box.has(event.target).length === 0 //checks if descendants of $box was clicked.
                        &&
                        !$box.is(event.target)) { //checks if the $box itself was clicked.
                        //$log.text("you clicked outside the box");
                        $(".project-link-open").find(".close_button"). trigger("click");
                        $(window).off(custom_events.clickOutsideProjectLink);
                    } else {
                        //$log.text("you clicked inside the box");
                    }
                });
            }


            // Set the click listener for all the .project-link elements.
            $(".project-link").on("click",showProject);

            // /PROJECT LINK CLICKS /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////




            /////////////////////////////////////////////////////////////////////////////
            // SCROLLING CONTROL ////////////////////////////////////////////////////////


            /*
             * This function manages the menu buttons to change the active button when scroll changes.
             * Calculates what is the section on the viewport according to isOnHalfScreen()
             * and set the corresponding menu button with the class "section-indicator-button--active".
             */
            function setActiveButton()  {
                // Every time we scroll we go throw all the #sectionXX elements to see if
                // they are on the viewport act manage the menu buttons accordingly.
                $(".project").each(function (index, value) {
                    // Select the menu button that correspond to each section using the index.
                    var mButtons = document.querySelectorAll("[data-id='" + pad(index,2) + "']");
                    // If the corresponding section is inside the viewport we will set the its related
                    // menu button as active.
                    if ($(value).isOnHalfScreen())  {
                        $(mButtons).addClass("section-indicator-button--active");
                    } else  {
                        $(mButtons).removeClass("section-indicator-button--active");
                    }
                });
            }


            // WINDOW ON SCROLL LISTENER.
            $(window).off("scroll");
            $(window).scroll(onScroll = function(e) {

                // Hide the logo when scrolling down.
                ($(this).scrollTop() < 100 && !menu_opened) ? $(".projects-logo").fadeIn(200) : $(".projects-logo").fadeOut(200);

                // Every time we scroll we check that the active side-menu dot corresponds with
                // the current visible project.
                clearTimeout($.data(this, 'scrollTimer2'));
                $.data(this, 'scrollTimer2', setTimeout(function() {
                    // Haven't scrolled in 150ms!
                    setActiveButton();
                }, 150));

                // CLOSING ANY OPEN FLUIDBOX OR PROJECT LINK:
                if (breakpoint != 'xs' && breakpoint != 'sm') { // only > md
                    //$(".project-image").trigger('close.fluidbox');
                    if ($(".project-link-open")) {
                        setTimeout(function () {
                            $(".project-link-open").find(".close_button").trigger("click");
                        }, 300);
                    }
                }

                // FADE IN ELEMENTS AS WE SCROLL DOWN.
                /* Check the location of each desired element */
                $('.project').each( function(i) {

                    let top_of_object = $(this).offset().top;
                    let bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object reached half of the visible window, fade it in. */
                    if (bottom_of_window > (top_of_object + $(window).height()/2)) {
                        // Adding this class sets opacity to 1 and margin-bottom to 0px.
                        // For the fadeIn & translate from bottom animation to work the
                        // elements need to be positioned throw the bottom attribute.
                        $(this).find(".project-image").addClass("fade-in-bottom");
                        $(this).find(".project-link").addClass("fade-in-bottom");
                    }
                });
            }); // End of $window onScroll.



            // PREVENT BODY SCROLL WHEN SCROLLING .project-link-addinfo-text DIV: DOMMouseScroll for firefox.
            if (breakpoint != 'xs' && breakpoint != 'sm') {
                $('.project-link-addinfo-text').on('mousewheel DOMMouseScroll', function (e) {
                    var event = e.originalEvent,
                        d = event.wheelDelta || -event.detail;
                    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
                    e.preventDefault();
                });
            }
            // /SCROLLING CONTROL////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////


            /////////////////////////////////////////////////////////////////////////////
            // CLICKS ON THE SIDE MENU BUTTONS --> Scroll to corresponding section. /////
            $("a.section-indicator-button").on('click', function(e) {
                e.preventDefault();
                var t = $('.project[data-index|=' + e.currentTarget.getAttribute("data-id") + ']');
                $(this).addClass("section-indicator-button--active");
                $('html, body').animate({ scrollTop: t.offset().top + 30}, 500, 'swing');
            });
            // /CLICKS ON THE MENU BUTTONS.
            /////////////////////////////////////////////////////////////////////////////


            /////////////////////////////////////////////////////////////////////////////
            // SET SIDE MENU ACTIVE BUTTON AFTER DOCUMENT READY:
            setActiveButton();
            /////////////////////////////////////////////////////////////////////////////


            /////////////////////////////////////////////////////////////////////////////
            // INITIALIZE FLUIDBOX //////////////////////////////////////////////////////
            /*
             * This function takes all the anchor elements and transform them into functional fluidBox instances.
             *
             * Fluidbox works by replacing your markup, which should be as follow:
             *
             * <a href="/path/to/image" title="">
             *      <img src="/path/to/image" alt="" title="" />
             * </a>
             *
             * by:
             *
             * <a href="/path/to/image" class="fluidbox fluidbox__instance-[i] fluidbox--closed">
             *      <div class="fluidbox__wrap" style="z-index: 990;">
             *          <img src="/path/to/image" alt="" title="" class="fluidbox__thumb" style="opacity: 1;">
             *          <div class="fluidbox__ghost" style="width: [w]; height: [h]; top: [t]; left: [l];"></div>
             *      </div>
             * </a>
             *
             * Where:
             *  i	Unique ID of the Fluidbox instance. New instances of Fluidbox will have new values.
             *  w	Computed width of the thumbnail, in pixels.
             *  h	Computed height of the thumbnail, in pixels.
             *  t	Computed offset, from the top and in pixels, of the thumbnail relative to its container.
             *  l	Computed offset, from the left and in pixels, of the thumbnail relative to its container.
             *
             * Each initialized Fluidbox instance can therefore be styled independently from each other using CSS alone,
             * requiring no further manipulation via JS (unless required on the user's behalf for specific
             * implementations). Fluidbox listens to the click event triggered on the ghost element, .fluidbox__ghost,
             * and toggles between two binary states, open or closed.
             */
            /*function initFluidBox(pic) {
                $(pic).fluidbox({
                        viewportFill: 0.9,
                        loader: true,
                        stackIndexDelta: 100
                        });
            }*/
            // If it is the first time we load projects section we will set
            // an onLoad event on every project-image to init fluidBox when
            // finishing loading. If the pics are already loaded we will init
            // fluidBox directly.
            /*$(".project-image > img").each(function() {
                if (this.complete)   {
                    initFluidBox($(this).parent());
                } else {
                    $(this).on("load", function(e) {
                        initFluidBox($(this).parent());
                        sessionStorage[section+'visited'] = true;
                    });
                }
            });*/
            // /INITIALIZE FLUIDBOX /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////




            // Close any open project-link or project-image when pressing
            // ESC on the keyboard.
            $(document).keyup(function(e) {
                if (e.keyCode === 27) { // escape key maps to keycode `27`
                    //$(".project-image").trigger('close.fluidbox');
                    $(".close_button:checked").trigger("click");
                }
            });






            /////////////////////////////////////////////////////////////////////////////
            // SET CONTACT BUTTON. //////////////////////////////////////////////////////
            /*
             * This functions append the corresponding html code to show
             * the contact button inside the $this element and adds the
             * ripple effect on click event.
             */
            function setContactButton($this) {

                // Append contact button's div:
                $($this).append('<div class="contact_button_container">' +
                    '<div class="contact-button animated-border hoverable" onclick="">' +
                    '<div class="anim"></div><span>Quiero Saber Más</span></div></div>');
                TweenMax.to($('.contact_button_container'), 0.5, {autoAlpha: 1}, 2);

                // Add Ripple effect on click event:
                $(".contact-button").click(function (e) {

                    // Remove any old one
                    $(".ripple").remove();

                    // Setup
                    var posX = $(this).offset().left,
                        posY = $(this).offset().top,
                        buttonWidth = $(this).width(),
                        buttonHeight = $(this).height();

                    // Add the element
                    $(this).prepend("<span class='ripple'></span>");


                    // Make it round!
                    if (buttonWidth >= buttonHeight) {
                        buttonHeight = buttonWidth;
                    } else {
                        buttonWidth = buttonHeight;
                    }

                    // Get the center of the element
                    var x = e.pageX - posX - buttonWidth / 2;
                    var y = e.pageY - posY - buttonHeight / 2;


                    // Add the ripples CSS and start the animation
                    $(".ripple").css({
                        width: buttonWidth,
                        height: buttonHeight,
                        top: y + 'px',
                       left: x + 'px'
                    }).addClass("rippleEffect");

                });

                $($this).find(".contact-button").on('click', setContactForm);

            }
            // /SET CONTACT BUTTON. /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////////////////
            // SET/HIDE CONTACT FORM. ////////////////////////////////////////////////////////
            /*
             * This functions append the corresponding html code to show
             * the contact form inside the $this element.
             */

            function setContactForm(e) {

                $(this).off('click', setContactForm);

                // We hide project text
                $(this).parent().parent().find(".project-link-addinfo-text").find(".project-link-addinfo-text-label").hide();
                // Project title
                var project_title = $(this).parent().parent().find(".project-link-title").text();
                // Append contact button's div:
                $(this).parent().parent().find(".project-link-addinfo-text-form").hide().html('<p><small>Solicita más información sobre este proyecto:</small></p><div id="formResponseProjects"></div><form id="contactForm">'
      + '<div class="row"><div class="col-12 col-md-6">'
      + '<div class="form-group">'
        + '<label class="control-label" for="form_name"><small>Nombre</small></label>'
        + '<div class="">'
        + '<input type="text" class="form-control form-control-sm" id="form_name" name="form_name" placeholder="Nombre">'
        + '</div>'
      + '</div>'
      + '<div class="form-group">'
        + '<label class="control-label" for="form_email"><small>Email</small></label>'
        + '<div class="">'
        + '<input type="email" class="form-control form-control-sm" id="form_email" name="form_email" aria-describedby="emailHelp" placeholder="Email">'
        + '</div>'
      + '</div>'
      + '<div class="form-group">'
        + '<label class="control-label" for="form_phone"><small>Teléfono</small></label>'
        + '<div class="">'
        + '<input type="tel" class="form-control form-control-sm" id="form_phone" name="form_phone" placeholder="Teléfono">'
        + '</div>'
      + '</div>'
      + '</div>'
      + '<div class="col-12 col-md-6">'
        + '<div class="form-group">' 
        + '<label class="control-label" for="form_message" class="control-label"><small>Mensaje</small></label>'
        + '<div class="">'
        + '<textarea class="form-control form-control-sm" id="form_message" name="form_message" rows="6"></textarea>'
        + '</div>'
        + '</div>'
        + '<input type="hidden" id="form_subject" name="form_subject">'
        + '<button type="submit" id="formSubmit" class="btn btn-light btn-lg btn-block"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar</button>'
      + '</div></div>'
                    + '</form>'
                    + '<script>addFormListener(); autoSizeOnShow();<\/script>').fadeIn();
                
                // We fill the hidden input with project title
                $("#form_subject").val(project_title);

                $(this).find("span").text('Volver');

                $(this).on('click', hideContactForm);
            }

            function hideContactForm(e) {

                $(this).off('click', hideContactForm);

                $(this).parent().parent().find(".project-link-addinfo-text-form").html('');
                $(this).parent().parent().find(".project-link-addinfo-text").find(".project-link-addinfo-text-label").fadeIn();
                $(this).parent().parent().find(".contact-button").find("span").text('Quiero Saber Más');

                $(this).on('click', setContactForm);
            }
            // /SET CONTACT FORM. ///////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////


            // OnPageChangeEvent (when loading a new section), we have to remove the mouseWheel listener
            // that scrolls the whole screen height no matter what the scroll was, so it wont affect other
            // sections.
            $('.page_container').on(custom_events.pageChangeEvent,function() {
                $(window).off("scroll", onScroll);
                $('.page_container').off(custom_events.pageChangeEvent);
            });



            // This block allow us to save a session variable to know if one section has already
            // been visited when we are visualizing any other loading any of the other sections.
            var visited = sessionStorage[section+'visited'];
            if (visited) {
                init();
            } else {
            }
            function init() {
            }

        });
        // /DOCUMENT READY BLOCK ///////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////

</script>

</script>
<!-- /Projects Page @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->