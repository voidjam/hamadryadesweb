<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- Contact Page  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<script id="contact_content" type="text/template">
    <div class="contact_content">

        <?php

        $language = 'es';
        $strings = array(
            ///////////////////////////////////////////////////////////////////////
            // SPANISH ////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////
            'es' => array(
                117   => '<strong>¡Hecho!</strong> Hemos enviado su email. Le contestaremos lo antes posible.',
                118   => '<strong>¡Error!</strong> Su email no ha podido ser enviado.<br>Por favor, intentelo de nuevo más tarde.'
            ),
            ///////////////////////////////////////////////////////////////////////
            // ENGLISH ////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////
            'en' => array(
                117   => '<strong>Done!</strong> Your email has been sent. We will answer you as soon as possible.',
                118   => '<strong>Error!</strong> Your email could not be sent. Please try again later.'
            )
        );

        ?>

        <!-- Response alert from contact form -->
        <div class="text-left" id="formResponse"></div>


        <div class="contact-text-header d-md-none"> <!-- ALV: needed for cp class to work -->
            <h2 id="cp"><span></span></h2>
            <h1 id="cp"><span>CONTACTO</span></h1>
        </div>

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- CONTACT INFO CONTAINER -->
        <div class="contact-info-wrapper">

            <!-- Texto hamadryades adjunto al logo -->
            <span class="logo_text d-none d-md-block" style="font-weight: 300; font-size:2rem; margin-left: 10px; color: #777;">hamadryades</span>

            <!-- Row pricipal con email, telefono y form -->
            <div class="flip-container" id="myCard" ontouchstart="this.classList.toggle('hover');"> <!-- ontouchstart="this.classList.toggle('hover');" -->
            <div class="flipper row mt-md-5">

                <!-- FRONT -->
                <!-- Email y telefono --->
                <div class="front row mt-4">
                    <div class="col-12 col-md-4 align-self-center text-center mb-3 mb-md-0">

                        <h1 class="page-title page-title--contact" title="Haga click aquí para enviarnos un mensaje.">
                            <a href="mailto:info@hmap.es" class="button email-button">
                                <i class="fa fa-envelope" aria-hidden="true"></i><br><span>info@hmap.es</span></a>
                        </h1>

                    </div>
                    <div class="col-12 col-md-4 align-self-center text-center mb-3 mb-md-0">

                        <h1 class="page-title page-title--contact">
                            <a href="tel:+34619029127" class="button email-button">
                                <i class="fa fa-phone" aria-hidden="true"></i><br><span>+34 619 029 127</span></a>
                        </h1>

                    </div>
                    <div class="col-12 col-md-4 align-self-center text-center mb-3 mb-md-0">

                        <h1 class="page-title page-title--contact">
                            <a href="#" class="writeus button writeus-button">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i><br><span>Escríbenos</span></a>
                        </h1>

                    </div>
                </div>
                <!-- /FRONT -->


                <!-- BACK -->
                <!-- Contact Form -->
                <div class="back mx-auto col-12">

                    <div class="px-md-4">

                        <form id="contactForm">
                        <div class="row mx-auto">

                            <div class="col-12 col-md-6">

                                <!-- Back Flip Return Button -->
                                <a href="#" class="d-md-none writeus email-button"><i class="fa fa-times closeForm" aria-hidden="true"></i></a>

                                <div class="form-group">
                                    <input type="text" class="form-control" tabindex="1" id="form_name" name="form_name" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" tabindex="2" id="form_email" name="form_email" placeholder="Email">
                                </div>
                                <div class="form-group mb-md-0">
                                    <input type="tel" class="form-control" tabindex="3" id="form_phone" name="form_phone" placeholder="Teléfono">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-md-0">

                                <!-- Back Flip Return Button -->
                                <a href="#" class="d-none d-md-block writeus email-button"><i class="fa fa-times closeForm" aria-hidden="true"></i></a>

                                <div class="form-group mb-md-0">
                                    <textarea class="form-control form-textarea" id="form_message" tabindex="4" name="form_message" placeholder="Mensaje"></textarea>
                                </div>
                            </div>
                            <div class="col-12 mb-4 mb-md-0 align-self-center">
                                <button type="submit" id="formSubmit" class="contact_submit btn btn-link btn-block mt-md-3"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar</button>
                            </div>

                        </div>
                        </form>

                    </div>

                </div>
                <!-- /BACK -->

            </div><!-- /FLIPPER-CONTAINER -->
        </div><!-- FLIPPER -->

              <!-- MAP & ADRESS IN DESKTOP -->
              <div class="address pb-4 pb-md-0 d-none d-md-block">
                  <div class="map" id="map"></div>
                  <div class="street">
                      <span>Calle Velázquez 15</span><br class="d-block d-md-none"><br>
                      <span>28001 Madrid</span><br>
                  </div>
              </div>

            <div class="d-md-none">
                <div class="address pb-3 mt-4">
                    <div class="map" id="map_mob"></div>
                    <div class="street text-center">
                        <span>Calle Velázquez 15</span><br class="d-block d-md-none"><br>
                        <span>28001 Madrid</span><br>
                    </div>
                </div>
            </div>

            <!-- Redes sociales -->
            <div class="d-md-none mb-5">
                <a href="https://twitter.com/hamadryades_map" class="icon icon-cube twitter mx-2" target="_blank">twitter</a>
                <a href="https://www.pinterest.es/hamadryades_map/" class="icon icon-cube pinterest mx-2" target="_blank">pinterest</a>
                <a href="https://plus.google.com/u/0/101036897474557739729" class="icon icon-cube googleplus mx-2" target="_blank">google+</a>
            </div>



            <!-- Umboweti Link -->
            <div class="home-link-jamboo d-md-none text-center" id="home-link-jamboo">
                <a href="http://umboweti.com" target="_blank"  title="Ir a la Página de los Creadores">Creado por Umboweti</a>
            </div>


            <!-- Copyright -->
            <div class="d-md-none w-100 text-center copyright-mob-contact"><span>©hamadryades 2017</span><span> | </span><span>info@hmap.es</span></div>

        </div> <!-- /.contact-info-wrapper -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- FOOTER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="copyright-contact d-none d-md-block">
            <div>©hamadryades 2017 | info@hmap.es</div>
        </div>


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- MOBILE: LOGO -->
        <div class="d-md-none contact-logo" onclick="showPage(page_section.home)">
            <!-- Include SVG logo code -->
            <div class="contact-logo-svg">
                <?php include($rootpath.'images/start_logo.svg'); ?>
            </div>
            <span>hamadryades</span>
        </div><!-- /projects-logo -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- MEDIA LINKS: DESKTOP -->
        <div class="media-link-container d-none d-md-block">
            <a href="https://twitter.com/hamadryades_map" class="icon icon-cube twitter" target="_blank">twitter</a>
            <a href="https://www.pinterest.es/hamadryades_map/" class="icon icon-cube pinterest" target="_blank">pinterest</a>
            <a href="https://plus.google.com/u/0/101036897474557739729" class="icon icon-cube googleplus" target="_blank">google+</a>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO -->
        <div class="page_section_info unselectable">
        <span>CONTACTO</span>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- UMBOWETI LINK -->
        <div class="home-link-jamboo d-none d-md-block" id="home-link-jamboo">
            <a href="http://umboweti.com" target="_blank"  title="Ir a la Página de los Creadores">Creado por Umboweti Studio</a>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    </div>


    <script type="text/javascript">

        // AJAX: SENDING EMAIL
        $("#contactForm").on('submit', function(e) {
            e.preventDefault();

            $('#formSubmit').attr("disabled", true);
            $.ajax({
                type: "POST",
                url: "<?php echo $rootpath; ?>libs/email-sender.php",
                //url: "http://132.148.91.154/~hamadryades/libs/email-sender.php",
                data: $(this).serialize(),
                success: function(response){
                    if (response != 'success') {
                        // WARNING
                        // we scroll to top
                        $('body').animate({
                            scrollTop: 0
                        });
                        $('#formSubmit').attr("disabled", false);
                        $("#formResponse").html(response);
                        $("#formResponse").fadeOut(0,function(){
                            $(this).html(response).fadeIn();
                        });
                    } else {
                        // SUCCESS
                        // we scroll to top
                        $('body').animate({
                            scrollTop: 0
                        });
                        $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification fade show" role="alert">'+
                            '<?php echo $strings[$language][117]; ?></div><\/script>'
                        );
                        $('#formSubmit').attr("disabled", false);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // ERROR
                    // we scroll to top
                    $('body').animate({
                        scrollTop: 0
                    });
                    $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification fade show" role="alert"><?php echo $strings[$language][118]; ?></div><\/script>'
                    );
                    $('#formSubmit').attr("disabled", false);
                    console.log(textStatus, errorThrown);
                }
            });
            // Auto close form alerts
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 5000);
        });


        // AUTOSIZE TEXTAREA ON SHOW
        var ta = document.querySelector('textarea');
        ta.style.display = 'none';
        autosize(ta);
        ta.style.display = '';
        // Call the update method to recalculate the size:
        autosize.update(ta);


        // Back Flip Div Link
        $(".writeus").click(function(e) {
            e.preventDefault();
            document.querySelector("#myCard").classList.toggle("flip");
        });


        // ON DOCUMENT READY.
        $(function() {

            // Hide the logo when scrolling down.
            $(window).off("scroll");
            $(".contact_content").scroll(function(e) {
                e.preventDefault();
                // Hide the logo when scrolling down.
                ($(this).scrollTop() < 50 && !menu_opened) ? $(".contact-logo").fadeIn(200) : $(".contact-logo").fadeOut(200);
            });

            // Google Maps Styles:
            let styles = [{
                featureType: "all",
                elementType: "labels",
                stylers: [{visibility: "off"}]
            }, {featureType: "all", elementType: "labels.text", stylers: [{visibility: "off"}]}, {
                featureType: "all",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "administrative",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "administrative.province",
                elementType: "all",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "administrative.locality",
                elementType: "labels.text",
                stylers: [{lightness: "-50"}, {visibility: "off"}]
            }, {
                featureType: "administrative.neighborhood",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "landscape",
                elementType: "all",
                stylers: [{saturation: -100}, {lightness: 45}, {visibility: "on"}]
            }, {
                featureType: "landscape",
                elementType: "geometry.fill",
                stylers: [{visibility: "on"}, {saturation: "0"}, {hue: "#ff0000"}]
            }, {
                featureType: "landscape",
                elementType: "labels.icon",
                stylers: [{visibility: "simplified"}]
            }, {
                featureType: "poi",
                elementType: "all",
                stylers: [{saturation: -100}, {lightness: 51}, {visibility: "off"}]
            }, {
                featureType: "poi.government",
                elementType: "all",
                stylers: [{visibility: "simplified"}]
            }, {
                featureType: "poi.government",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "poi.medical",
                elementType: "all",
                stylers: [{visibility: "simplified"}]
            }, {
                featureType: "poi.medical",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road",
                elementType: "all",
                stylers: [{saturation: "-100"}, {lightness: "0"}]
            }, {
                featureType: "road",
                elementType: "labels.text",
                stylers: [{lightness: "0"}, {visibility: "off"}]
            }, {
                featureType: "road",
                elementType: "labels.icon",
                stylers: [{lightness: "50"}, {visibility: "off"}]
            }, {
                featureType: "road.highway",
                elementType: "all",
                stylers: [{visibility: "simplified"}]
            }, {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [{color: "#fff"}]
            }, {
                featureType: "road.highway",
                elementType: "labels",
                stylers: [{lightness: "0"}]
            }, {
                featureType: "road.highway",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road.highway",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}, {lightness: "0"}]
            }, {
                featureType: "road.highway.controlled_access",
                elementType: "geometry.fill",
                stylers: [{color: "#fff"}]
            }, {
                featureType: "road.highway.controlled_access",
                elementType: "labels",
                stylers: [{lightness: "0"}]
            }, {
                featureType: "road.highway.controlled_access",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road.highway.controlled_access",
                elementType: "labels.icon",
                stylers: [{lightness: "-10"}, {saturation: "0"}, {visibility: "off"}]
            }, {
                featureType: "road.arterial",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road.local",
                elementType: "all",
                stylers: [{visibility: "on"}, {lightness: "41"}, {saturation: "0"}]
            }, {
                featureType: "road.local",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "transit",
                elementType: "all",
                stylers: [{saturation: -100}, {visibility: "simplified"}]
            }, {
                featureType: "transit.line",
                elementType: "geometry.fill",
                stylers: [{lightness: "0"}]
            }, {
                featureType: "transit.station.airport",
                elementType: "labels.text",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "transit.station.airport",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "transit.station.bus",
                elementType: "all",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "transit.station.rail",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {featureType: "water", elementType: "geometry", stylers: [{color: "#dce6e6"}]}, {
                featureType: "water",
                elementType: "labels",
                stylers: [{visibility: "on"}, {lightness: -25}, {saturation: -100}]
            }, {featureType: "water", elementType: "labels.text", stylers: [{lightness: "50"}, {visibility: "off"}]}];


            let icon = {
                url: "<?php echo $rootpath; ?>images/logo_vertical.svg",
                scaledSize: new google.maps.Size(22, 40)
            };

            let div = document.getElementById('map');
            let div_mob = document.getElementById('map_mob');

            function initMap() {

                //let myLatlng = {lat: 40.4314, lng: -3.6864}; // serrano
                let myLatlng = {lat: 40.4227308, lng: -3.6846112}; // Calle Velazquez 15
                let center = {lat: 40.4238, lng: -3.672666};
                let center_mob = {lat: 40.4238, lng: -3.676};

                let map = new google.maps.Map(div, {
                    center: center,
                    zoom: 14,
                    styles
                });

                let map_mob = new google.maps.Map(div_mob, {
                    center: center_mob,
                    zoom: 14,
                    styles
                });

                let marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    title: 'Hamadryades',
                    icon: icon,
                    url: "https://goo.gl/maps/yA45vnei5tj"
                });

                let marker_mob = new google.maps.Marker({
                    position: myLatlng,
                    map: map_mob,
                    animation: google.maps.Animation.DROP,
                    title: 'Hamadryades',
                    icon: icon,
                    url: "https://goo.gl/maps/yA45vnei5tj"
                });

                map.addListener('center_changed', function () {
                    // 3 seconds after the center of the map has changed, pan back to the
                    // marker.
                    window.setTimeout(function () {
                        map.panTo(center);
                        map.setZoom(14);
                    }, 4000);
                });

                map_mob.addListener('center_changed', function () {
                    // 3 seconds after the center of the map has changed, pan back to the
                    // marker.
                    window.setTimeout(function () {
                        map_mob.panTo(center_mob);
                        map_mob.setZoom(14);
                    }, 4000);
                });

                map.addListener('click', function () {
                    window.open(marker.url);
                });

                map_mob.addListener('click', function () {
                    window.open(marker_mob.url);
                });
            }

            initMap();
        });
    </script>
</script><!-- /Contact; Page;  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->