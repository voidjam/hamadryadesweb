<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- About Page @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<script id="about_content" type="text/template">
    <div class="about_content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION 00 -> INTRO -->
        <section id="section00" class="section">
            <div class="about_text_header">
                <h2 id="cp"><span>Nuestro</span></h2>
                <h1 id="cp"><span>ESTUDIO</span>
                </h1>
            </div>

            <div class="about_text_container">
                <div class="row about_text_row mx-auto">
                    <div class="col-12 col-md-6 about_text_column">
                        <p><strong>Estudio Hamadryades</strong> está dirigido y
                        gestionado por <strong>Susana Díaz-Palacios Sisternes</strong> y <strong>Francisco Lafuente Terceros</strong>.
                        Nuestra formación y experiencia en el sector de la <strong>ingeniería agronómica</strong>, la
                        <strong>arquitectura</strong> y el <strong>paisajismo</strong> ofrece a los clientes del estudio
                        una visión global, técnica y altamente especializada en diseño y ejecución de jardines, estudios
                        de paisaje, <strong>proyectos agrarios y medioambientales</strong>.</p></div>
                    <div class="col-12 col-md-6 about_text_column">
                        <p> Desde 2001, el estudio lleva desarrollando <strong>trabajos y soluciones personales y a medida</strong>,
                            tanto para empresas como para clientes particulares. Todo, a muy diferentes escalas y
                            niveles de intervención. Los proyectos abarcan desde los <strong>estudios y análisis iniciales</strong>
                            necesarios hasta la <strong>tramitación administrativa</strong> y <strong>legal</strong> requerida en cada caso,
                            así como el <strong>desarrollo integral</strong> de cada trabajo en cuestión con su posterior proceso de seguimiento.
                        </p>
                    </div>
                </div>
            </div>


            <!-- ARROW LINKS TO SERVICES -->
            <div class="about_text_row scroll_buttons d-none d-md-block">
                <!-- Scroll down button -->
                <!-- Span element is the spinning arrow -->
                <div class="about_text_column scroll_down_button">
                    <a href="#section01"><span></span>Susana Díaz – Palacios Sisternes</a>
                </div>
                <!-- Scroll down button -->
                <!-- Span element is the spinning arrow -->
                <div class="about_text_column scroll_down_button">
                    <a href="#section02"><span></span>Francisco Lafuente Terceros</a>
                </div>
            </div>

        </section>
        <!-- /SECTION 00 -> INTRO -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION 01 -> SUSANA -->
        <section id="section01" class="section">
            <h1 class="d-none d-md-block">Susana Díaz-Palacios Sisternes</h1>

            <div class="container-fluid container"><div class="row">
                <div class="column col-12 col-md-5 align-self-center text-center">
                    <img class="susana_img_mob d-md-none" src="./images/sdps_color.jpg" alt="Susana Díaz-Palacios Sisternes"  title="Ver LinkedIn" onclick="window.open('https://www.linkedin.com/in/susana-diaz-palacios-sisternes-0752b922','new_window');"/>
                    <div class="susana_img d-none d-md-block">
                        <img class="grayscale" src="./images/susana_color.jpg" alt="Susana Díaz-Palacios Sisternes" title="Ver LinkedIn" onclick="window.open('https://www.linkedin.com/in/susana-diaz-palacios-sisternes-0752b922','new_window');"/>
                    </div>
                </div>
                <div class="column col">
                    <h1 class="d-block d-md-none">Susana Díaz-Palacios Sisternes</h1>
                    <span>Dra. Ingeniero Agrónomo y Máster en Jardinería y Paisajismo por la Universidad Politécnica de Madrid.<br>Mi gran pasión es el paisaje: espacio que comienza en un jardín y se extiende hasta el infinito, más allá de sus muros. Como paisajista, trabajo siempre desde un enfoque holístico, integrando en cada proyecto los procesos ecológicos con los propiamente humanos.</span>

                    <br class="d-none d-md-block">

                    <blockquote>
                        Me he especializado en el desarrollo de metodologías de análisis del paisaje visual, valoración cuantitativa de impactos paisajísticos y dinámica del paisaje. Tengo una especial predilección por aquellos paisajes que sufren los procesos de <a target="_blank" href="http://www.comunidadism.es/blogs/sinantropizacion"><em>sinantropización</em></a> más intensos.
                    </blockquote>
                </div>
            </div></div>


            <!-- Scroll down button -->
            <!-- Span element is the spinning arrow -->
            <div class="scroll_down_button d-none d-md-block">
                <a href="#section02"><span></span>Francisco Lafuente Terceros</a>
            </div>
        </section>
        <!-- /SECTION 01 -> SUSANA -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION 02 -> PANCHO -->
        <section id="section02" class="section">
            <h1 class="d-none d-md-block">Francisco Lafuente Terceros</h1>

            <div class="container-fluid container"> <!-- If Needed Left and Right Padding in 'md' and 'lg' screen means use container class -->
                <div class="row">
                    <div class="column col">
                        <h1 class="d-block d-md-none">Francisco Lafuente Terceros</h1>
                        <span>Arquitecto por la Universidad Técnica de Oruro, Máster en Conservación y Restauración del Patrimonio Arquitectónico y Urbano por la Universidad Politécnica de Madrid.<br>Como arquitecto, me interesa considerar al ser humano en toda su complejidad para poder ofrecer una solución espacial a todo aquello que acontece en su hábitat.</span>

                        <br class="d-none d-md-block">

                        <blockquote>
                            He tenido la oportunidad de participar en proyectos a diferentes escalas de intervención, lo que me ha llevado a fundamentar mi forma de trabajar en la funcionalidad, la memoria y la integración ambiental. Mi deseo es poder ofrecer soluciones personalizadas que consideren <em>el más mínimo detalle</em>.
                        </blockquote>
                    </div>

                    <div class="column col-12 col-md-5 order-first order-md-2 align-self-center text-center">
                        <img class="pancho_img_mob d-md-none" src="./images/flt_color.jpg" alt="Francisco Lafuente Terceros" title="Ver LinkedIn" onclick="window.open('https://www.linkedin.com/in/francisco-jesus-lafuente-terceros-\n'+
'706271116','new_window');"/>
                        <div class="pancho_img d-none d-md-block">
                            <img class="grayscale" src="./images/pancho_color.jpg" alt="Francisco Lafuente Terceros" title="Ver LinkedIn" onclick="window.open('https://www.linkedin.com/in/francisco-jesus-lafuente-terceros-\n'+
'706271116','new_window');"/>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Scroll down button -->
            <!-- Span element is the spinning arrow -->
            <div class="scroll_down_button up d-none d-md-block">
                <a href="#section00"  title="Volver arriba."><span></span></a>
            </div>


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- FOOTER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <div class="footer">
                <div class="text-white m-4" style="bottom: 0; right: 0; position: absolute;">©hamadryades 2017 | info@hmap.es</div>
                <img src="./images/footer_bn.png"/>
            </div>
            <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        </section>
        <!-- /SECTION 02 -> PANCHO -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- COPYRIGHT INFO -->
        <div class="d-md-none mb-3" style="text-align: center"><span>©hamadryades 2017</span><span> | </span><span>info@hmap.es</span></div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    </div>
    <!-- /div class="about_content" -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- LOGO -->
    <div class="about-logo" onclick="showPage(page_section.home)">
        <!-- Include SVG logo code -->
        <div class="about-logo-svg">
            <?php include($rootpath.'images/start_logo.svg'); ?>
        </div>
        <span>hamadryades</span>
    </div><!-- /services-logo -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SIDE DOTS MENU ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="section-indicator about-section-indicator">
    <ul class="section-indicator-list">
        <li class="list-item list-item-section-indicator">
            <a class="button section-indicator-button section-indicator-button--active" data-id="00">
                <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                <span class="label section-indicator-label">Intro</span>
            </a>
        </li>
        <li class="list-item list-item-section-indicator">
            <a class="button section-indicator-button" data-id="01">
                <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                <span class="label section-indicator-label">Susana&nbsp;Díaz-Palacios&nbsp;Sisternes</span>
            </a>
        </li>
        <li class="list-item list-item-section-indicator">
            <a class="button section-indicator-button" data-id="02">
                <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                <span class="label section-indicator-label">Francisco&nbsp;Lafuente&nbsp;Terceros</span>
            </a>
        </li>
    </ul>
    </div>
    <!-- /SIDE DOTS MENU +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span>NOSOTROS</span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- Scroll sections control -->
    <script type="text/javascript" visivility="hidden">

        var handleWheel;

        ////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////
        $(function() {

            // An array with al the #sectionXX elements.
            var sections = $('[id^=section]');

            /*
             * This function manages the menu buttons to change the active button when scroll changes.
             * Calculates what is the section on the viewport according to isOnHalfScreen()
             * and set the corresponding menu button with the class "section-indicator-button--active".
             */
            function setActiveButton()  {
                // Every time we scroll we go throw all the #sectionXX elements to see if
                // they are on the viewport act manage the menu buttons accordingly.
                sections.each(function (index, value) {
                    // Select the menu button that correspond to each section using the index.
                    var mButtons = document.querySelectorAll("[data-id='" + pad(index,2) + "']");
                    // If the corresponding section is inside the viewport we will set the its related
                    // menu button as active.
                    if ($(value).isOnHalfScreen())  {
                        $(mButtons).addClass("section-indicator-button--active");
                    } else  {
                        $(mButtons).removeClass("section-indicator-button--active");
                    }
                });
            }

            // We call it on the document start in case the scroll is not on the top of the page.
            setActiveButton();

            // UPDATE THE MENU BUTTONS AFTER ANY SCROLL.
            // On scroll we detect if the section on the viewport changed and set the
            // corresponding menu button. This event is not triggered when we use the
            // mouse wheel, only when dragging the scroll bar or using the arrows.
            $(window).off("scroll");
            $(window).scroll(function(e) {
                e.preventDefault();

                // Hide the logo when scrolling down.
                ($(this).scrollTop() < 100 && !menu_opened) ? $(".about-logo").fadeIn(200) : $(".about-logo").fadeOut(200);

                setActiveButton();
            });


            // CLICK ON SCROLL DOWN BUTTONS (NOT MENU) --> Scroll between sections.
            // The selector # is not valid. It is a special char and needs to be escaped like 'a[href*=\\#]:not([href=\\#])'
            $("a[href*=\\#]").on('click', function(e) {
                e.preventDefault();
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'swing');
            });


            // CLICKS ON THE MENU BUTTONS --> Scroll between sections.
            $("a.section-indicator-button").on('click', function(e) {
                e.preventDefault();
                var t = $('#section' + e.currentTarget.getAttribute("data-id"));
                $(this).addClass("section-indicator-button--active");
                $('html, body').animate({ scrollTop: t.offset().top}, 500, 'swing');
            });



            // ADDING MOUSE WHEEL LISTENER --> If the scroll up/down was triggered by the mouse wheel,
            // we will not scroll any deltaY but directly scroll all the way to the previous/next section.
            // Based on http://www.sitepoint.com/html5-javascript-mouse-wheel/
            var enventFired = false;
            handleWheel = function (event) {
                event.preventDefault();

                // If a wheel event has been fired and not finished we will do nothing.
                if (enventFired) return;

                enventFired = true;
                removeMouseWheelEventListener(handleWheel);
                // cross-browser wheel delta
                // Chrome / IE: both are set to the same thing - WheelEvent for Chrome, MouseWheelEvent for IE
                // Firefox: first one is undefined, second one is MouseScrollEvent
                var e = window.event || event;
                // Chrome / IE: first one is +/-120 (positive on mouse up), second one is zero
                // Firefox: first one is undefined, second one is -/+3 (negative on mouse up)
                var delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));

                var currSection;
                sections.each(function (index, value) {
                    // If the corresponding section is inside the viewport we will set the its related
                    // menu button as active.
                    if ($(value).isOnHalfScreen())  {
                        currSection = index;
                    }
                });

                // Depending if the wheel scroll was up or down we will scroll to the next
                // or the previous section:
                // Scroll UP and visible section is not #section00.
                if(delta > 0 && currSection > 0 ) {
                    $('html, body').animate({ scrollTop:  $(sections[currSection - 1]).offset().top }, 200, 'swing', addMouseWheelEventListener(handleWheel));
                }
                // Scroll DOWN and visible section is not #section02.
                else if (delta < 0 && currSection < (sections.length -1)) {
                    $('html, body').animate({ scrollTop:  $(sections[currSection + 1]).offset().top }, 200, 'swing',addMouseWheelEventListener(handleWheel));
                // If we are in the first or the last section we will just reset the wheel event listener.
                } else {
                    addMouseWheelEventListener(handleWheel);
                }

                // Set that the current wheel event finished and we are ready to listen to another one
                setTimeout(function() { enventFired = false; },600);
            };

            // console.log("Adding HandleWheel Scroll Listener");
            if (breakpoint != 'xs' && breakpoint != 'sm') { // only in desktop version
                addMouseWheelEventListener(handleWheel);
            }


            // BLACK AND WHITE IMG EFFECT --> The imgs with class="grayscale" will be showed on
            // black and white and coloured when the mouse goes inside the picture. For this we
            // will run removeColors() when the picture has been load.
            function showColorImg() {
                this.style.display = "none";
                this.nextSibling.style.display = "inline";
            }

            function showGrayImg() {
                this.previousSibling.style.display = "inline";
                this.style.display = "none";
            }

            /*
             * This functions is fired by an img.onload event and will remove
             * the color from the img element showing the coloured picture on
             * mouse's hover.
             *
             * We can also pass a img elements array to remove the color.
             */
            function removeColors(event, imgArray = null) {


                if (imgArray != null) {var aImages = imgArray;}
                else { var aImages = $(event.target);} //$() puts the element into an array, euqual to [event.target]


                var nImgsLen = aImages.length,
                    oCanvas = document.createElement("canvas"),
                    oCtx = oCanvas.getContext("2d");

                for (var nWidth, nHeight, oImgData, oGrayImg, nPixel, aPix, nPixLen, nImgId = 0; nImgId < nImgsLen; nImgId++) {
                    oColorImg = aImages[nImgId];
                    nWidth = oColorImg.offsetWidth;
                    nHeight = oColorImg.offsetHeight;
                    oCanvas.width = nWidth;
                    oCanvas.height = nHeight;
                    oCtx.drawImage(oColorImg, 0, 0);
                    oImgData = oCtx.getImageData(0, 0, nWidth, nHeight);
                    aPix = oImgData.data;
                    nPixLen = aPix.length;

                    for (nPixel = 0; nPixel < nPixLen; nPixel += 4) {
                      aPix[nPixel + 2] = aPix[nPixel + 1] = aPix[nPixel] = (aPix[nPixel] + aPix[nPixel + 1] + aPix[nPixel + 2]) / 3;
                    }

                    oCtx.putImageData(oImgData, 0, 0);
                    oGrayImg = new Image();
                    oGrayImg.src = oCanvas.toDataURL();
                    oGrayImg.onmouseover = showColorImg;
                    oColorImg.onmouseout = showGrayImg;
                    oCtx.clearRect(0, 0, nWidth, nHeight);
                    oColorImg.style.display = "none";
                    oColorImg.parentNode.insertBefore(oGrayImg, oColorImg);
              }
            }


            // RemoveColors() when the picture has been load.
            $(".grayscale").each(function() {
                if (breakpoint != 'xs' && breakpoint != 'sm') { // black and white only in desktop
                    if (this.complete) {
                        removeColors(null, $(this));
                    } else {
                        $(this).on("load", function (e) {
                            removeColors(e);
                            sessionStorage[section + 'visited'] = true;
                        });
                    }
                }
            });


            // OnPageChangeEvent (when loading a new section), we have to remove the mouseWheel listener
            // that scrolls the whole screen height no matter what the scroll was, so it wont affect other
            // sections.
            $('.page_container').on(custom_events.pageChangeEvent,function() {
                //console.log("Leaving About Page: Remove handleWheel");
                removeMouseWheelEventListener(handleWheel);
                $('.page_container').off(custom_events.pageChangeEvent);
            });


            // This block allow us to save a session variable to know if one section has already
            // been visited when we are visualizing any other loading any of the other sections.
            var visited = sessionStorage[section+'visited'];
            if (visited) {
                init();
            }
            function init() {

            }



        });
        // /DOCUMENT READY BLOCK ///////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////

    </script>
</script>
<!-- /About; Page; @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->