<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- Services Page @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<script id="services_content" type="text/template">
    <div class="services_content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION00 - INTRO -->
        <div class="services_text_container" id="section00">

            <!-- PAGE TITTLE -->
            <div class="about_text_header services_text_header"> <!-- ALV: needed for cp class to work -->
                <h2 id="cp"><span>Nuestros</span></h2>
                <h1 id="cp"><span>SERVICIOS</span>
                </h1>
            </div>

            <!-- INTRO TEXT -->
            <div class="services_text_row">
                <div class="services_text_column">
                    <p>Nuestros servicios responden a las <strong>diferentes necesidades</strong> de una gran y variada clientela cada vez
                        más exigente. Trabajamos desde una escala territorial, para <strong>grandes empresas y organismos públicos</strong>,
                        hasta escalas menores para <strong>arquitectos, interioristas o particulares</strong>.
                        </br></br>
                        Para ofrecer <strong>soluciones personalizadas</strong> al problema de cada cliente, hemos desarrollado
                        <strong>metodologías de trabajo totalmente originales</strong>, utilizando para ello herramientas de análisis y diseño
                        (Sistemas Información Geográfica – ArcGIS de ESRI -, Diseño – Sketchup, Vray, AutoARQ Paisajismo y
                        Corel Draw).
                    </p>
                </div>
                <div class="services_text_column">
                    <p>
                        En el área de <strong>paisajismo</strong>, contamos con personal propio para la ejecución de nuestros proyectos,
                        así como con un equipo de colaboradores de acreditada solvencia pertenecientes a diferentes oficios
                        y proveedores de materiales reconocidos en el mercado.
                        Además, realizamos un <strong>seguimiento y control de calidad</strong> minucioso tanto en el proceso de ejecución
                        como tras la finalización del proyecto.
                    </p>
                </div>
            </div>
            <div class="services_text_row mb-5 mb-md-0">
                    </br><strong>Trabajamos para quienes que buscan rigor técnico, soluciones holísticas y una ejecución de calidad.</strong>
            </div>
            <div class="services_text_row mb-5 mb-md-0">
                </br><strong><h3>¡Nos has encontrado!</h3></strong>
            </div>



            <!-- ARROW LINKS TO SERVICES -->
            <div class="services_text_row d-none d-md-block"> <!-- only desktop -->
                <div class="scroll_down_button">
                    <a href="#section01"><span></span>Estudios de Paisaje</a>
                </div>
                <div class="scroll_down_button">
                    <a href="#section02"><span></span>Paisajismo</a>
                </div>
                <div class="scroll_down_button">
                    <a href="#section03"><span></span>Agronomía y Agricultura Urbana</a>
                </div>
                <div class="scroll_down_button">
                    <a href="#section04"><span></span>Medio Ambiente</a>
                </div>
                <div class="scroll_down_button">
                    <a href="#section05"><span></span>Investigación y Docencia</a>
                </div>
            </div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION01 - ESTUDIOS DE PAISAJE -->
        <div class="services-section odd" id="section01">
            <h1 class="d-none d-md-block">Estudios de Paisaje</h1>
            <div class="container-fluid container"><div class="row">
                    <div class="column col-12 col-md-5 align-self-center">
                        <div class="services-icon" title="Ver Proyectos" onclick="showPage(page_section.projects, false, 'paisaje')">
                            <img class="grayscale" src="./images/icons/paisaje_CI.png"/>
                        </div>
                    </div>
                    <div class="column col align-self-center">
                        <h1 class="d-block d-md-none">Estudios de Paisaje</h1>
                        <blockquote>
                            Realizamos estudios de impacto e integración
                            paisajística en sectores como el urbanismo, las
                            infraestructuras, la agronomía y la arquitectura.
                            Desarrollamos metodologías que integran la
                            dimensión ecológica, humana y temporal que
                            caracteriza a todo paisaje. <a onclick="showPage(page_section.projects, false, 'paisaje')">Ver proyectos</a>.
                        </blockquote></br class="d-none d-md-block">
                    </div>
                </div></div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION02 - PAISAJISMO -->
        <div class="services-section even" id="section02">
            <h1 class="d-none d-md-block">Paisajismo</h1>
            <div class="container-fluid container"><div class="row">
                    <div class="column col align-self-center">
                        <h1 class="d-block d-md-none">Paisajismo</h1>
                        <blockquote>
                            La formación complementaria de nuestro equipo
                            permite integrar en cada proyecto un diseño de
                            calidad de forma eficaz y real, ecológicamente
                            integrado y ejecutado con la máxima profesionalidad.
                            <br>
                            Para respetar y potenciar el dinamismo, vertebramos
                            su diseño en torno a los principios de sostenibilidad y
                            del mantenimiento diferenciado, ya que
                            consideramos que nuestro trabajo no finaliza a la
                            firma del cierre de obra.<br>
                            Disfrutamos y aportamos soluciones adaptadas al
                            lugar y al cliente, independientemente de la escala
                            de intervención: trabajamos con la misma pasión en
                            el diseño y ejecución de un parque, que en el de un
                            huerto o pequeño jardín de un cliente particular.
                            <a onclick="showPage(page_section.projects, false, 'paisajismo')">Ver
                                proyectos</a>.
                        </blockquote>
                    </div>
                    <div class="column col-12 col-md-5 order-first order-md-2 align-self-center">
                        <div class="services-icon" title="Ver Proyectos" onclick="showPage(page_section.projects, false, 'paisajismo')">
                            <img class="grayscale" src="./images/icons/paisajismo_CI.png"/>
                        </div>
                    </div>
                </div></div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION03 - AGRONOMIA Y AGRICULTURA URBANA -->
        <div class="services-section odd" id="section03">
            <h1 class="d-none d-md-block">Agronomía y Agricultura Urbana</h1>
            <div class="container-fluid container"><div class="row">
                    <div class="column col-12 col-md-5 align-self-center">
                        <div class="services-icon" title="Ver Proyectos" onclick="showPage(page_section.projects, false, 'agro')">
                            <img class="grayscale" src="./images/icons/agro_CI.png"/>
                        </div>
                    </div>
                    <div class="column col align-self-center">
                        <h1 class="d-block d-md-none">Agronomía y Agricultura Urbana</h1>
                        <blockquote>
                            A lo largo del proceso de cada proyecto, consideramos los vectores ambientales como
                            oportunidades de diseño. Esta metodología de trabajo facilita su tramitación medioambiental,
                            ya que estamos seguros de que la mejor medida correctora se basa en desarrollar un diseño
                            ambientalmente integrado. Ver proyectos – (enlace a la página de proyectos)<br>
                            Nuestra preocupación por el cambio climático y los problemas de seguridad alimentaria
                            derivados del incremento de la población urbana frente a la rural a escala mundial,
                            unido a la desvinculación cada vez mayor de la humanidad con la naturaleza,
                            nos ha llevado a desarrollar soluciones integrales de agricultura urbana.
                            <a onclick="showPage(page_section.projects, false, 'agro')">Ver proyetos</a>
                        </blockquote>
                    </div>
                </div></div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION04 - MEDIO AMBIENTE -->
        <div class="services-section even" id="section04">
            <h1 class="d-none d-md-block">Medio Ambiente</h1>
            <div class="container-fluid container"><div class="row">
                    <div class="column col align-self-center">
                        <h1 class="d-block d-md-none">Medio Ambiente</h1>
                        <blockquote>
                            En los últimos años, hemos implementado metodologías de análisis y minoración de
                            impactos en sectores como el urbanismo, la energía, infraestructuras,
                            agronomía y arquitectura, siendo especialistas en evaluación ambiental de planes,
                            programas y proyectos. <a onclick="showPage(page_section.projects, false, 'mma')">Ver proyectos</a>
                        </blockquote>
                    </div>
                    <div class="column col-12 col-md-5 order-first order-md-2 align-self-center">
                        <div class="services-icon" title="Ver Proyectos" onclick="showPage(page_section.projects, false, 'mma')">
                            <img class="grayscale" src="./images/icons/mma_CI.png"/>
                        </div>
                    </div>
                </div></div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION05 - INVESTIGACIÓN Y DOCENCIA -->
        <div class="services-section odd" id="section05">
            <h1 class="d-none d-md-block">Investigación y Docencia</h1>
            <div class="container-fluid container"><div class="row">
                    <div class="column col-12 col-md-5 align-self-center">
                        <div class="services-icon">
                            <img class="grayscale" src="./images/icons/world_investigacion_CI.png"/>
                        </div>
                    </div>
                    <div class="column col align-self-center">
                        <h1 class="d-block d-md-none">Investigación y Docencia</h1>
                        <blockquote>
                            Además de estos estudios como consultoría, nuestra experiencia de años sobre el terreno y
                            nuestra pasión por nuestra profesión nos han llevado a reflexionar e impulsar nuestras
                            propias líneas de investigación y desarrollo; muchas de ellas recogidas en la tesis 
                            <a href="http://oa.upm.es/32626/" target="_blank">Análisis
                            espacio-temporal del gradiente urbano-rural del sur de la Región Metropolitana de Madrid y
                            su entorno. Caracterización de los procesos y patrones paisajísticos
                            acaecidos en el período 1990-2006</a>, que recibió el Premio José Cascón a la mejor
                            tesis doctoral, convocatoria 2015, otorgada por el Colegio Oficial de Ingenieros Agrónomos
                            de Centro y Canarias. Parte de los resultados han sido publicados en la prestigiosa revista
                            <a href="http://www.sciencedirect.com/science/article/pii/S0264275114000456" target="_blank">Cities</a>, de
                            Elsevier, con reconocimiento mundial.<br><br>
                            Hemos participado en varios proyectos de investigación a nivel europeo y acudido como
                            conferenciantes y expertos a foros como la Fundación COAM. Compatibilizamos esta actividad
                            con la docente, habiendo impartido materias de grados y post-grado para arquitectos,
                            paisajistas, licenciados en ciencias ambientales, biólogos o geógrafos en centros y
                            universidades como la Politécnica de Madrid.
                        </blockquote>
                    </div>
                </div></div>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- FOOTER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="footer">
            <div class="text-white m-4" style="bottom: 0; right: 0; position: absolute;">©hamadryades 2017 | info@hmap.es</div>
            <img src="./images/footer_bn.png"/>
        </div>
        <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="d-lg-none copyright-mob-services text-center"><span>©hamadryades 2017</span><span> | </span><span>info@hmap.es</span></div>

    </div> <!-- /services_content -->



    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- LOGO -->
    <div class="projects-logo services-logo" onclick="showPage(page_section.home)">
        <!-- Include SVG logo code -->
        <div class="projects-logo-svg">
            <?php include($rootpath.'images/start_logo.svg'); ?>
        </div>
        <span>hamadryades</span>
    </div><!-- /services-logo -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SIDE DOTS MENU ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="section-indicator">
        <ul class="section-indicator-list">
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button section-indicator-button--active" data-id="00">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Intro</span>
                </a>
            </li>
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button" data-id="01">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Estudios de Paisaje</span>
                </a>
            </li>
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button" data-id="02">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Paisajismo</span>
                </a>
            </li>
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button" data-id="03">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Agronomía y Agricultura Urbana</span>
                </a>
            </li>
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button" data-id="04">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Medio Ambiente</span>
                </a>
            </li>
            <li class="list-item list-item-section-indicator">
                <a class="button section-indicator-button" data-id="05">
                    <span class="dot section-indicator-dot section-indicator-dot-normal"></span>
                    <span class="dot section-indicator-dot section-indicator-dot-pulse"></span>
                    <span class="label section-indicator-label">Investigación y Docencia</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- /SIDE DOTS MENU +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span>SERVICIOS</span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->









    <script type="text/javascript">

        ////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////
        $(function() {

            // An array with al the #sectionXX elements.
            var sections = $('[id^=section]');

            /*
             * This function manages the menu buttons to change the active button when scroll changes.
             * Calculates what is the section on the viewport according to isOnHalfScreen()
             * and set the corresponding menu button with the class "section-indicator-button--active".
             */
            function setActiveButton()  {
                // Every time we scroll we go throw all the #sectionXX elements to see if
                // they are on the viewport act manage the menu buttons accordingly.
                sections.each(function (index, value) {
                    // Select the menu button that correspond to each section using the index.
                    var mButtons = document.querySelectorAll("[data-id='" + pad(index,2) + "']");
                    // If the corresponding section is inside the viewport we will set the its related
                    // menu button as active.
                    if ($(value).isOnHalfScreen())  {
                        $(mButtons).addClass("section-indicator-button--active");
                    } else  {
                        $(mButtons).removeClass("section-indicator-button--active");
                    }
                });
            }

            // We call it on the document start in case the scroll is not on the top of the page.
            setActiveButton();


            // UPDATE THE MENU BUTTONS AFTER ANY SCROLL.
            // On scroll we detect if the section on the viewport changed and set the
            // corresponding menu button. This event is not triggered when we use the
            // mouse wheel, only when dragging the scroll bar or using the arrows.
            $(window).off("scroll");
            $(window).on("scroll", function(e) {
                e.preventDefault();

                // Hide the logo when scrolling down.
                ($(this).scrollTop() < 100 && !menu_opened) ? $(".services-logo").fadeIn(200) : $(".services-logo").fadeOut(200);

                setActiveButton();
            });


            // CLICK ON SCROLL DOWN BUTTONS (NOT MENU) --> Scroll between sections.
            // The selector # is not valid. It is a special char and needs to be escaped like 'a[href*=\\#]:not([href=\\#])'
            $("a[href*=\\#]").on('click', function(e) {
                e.preventDefault();
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'swing');
            });


            // CLICKS ON THE MENU BUTTONS --> Scroll between sections.
            $("a.section-indicator-button").on('click', function(e) {
                e.preventDefault();
                var t = $('#section' + e.currentTarget.getAttribute("data-id"));
                $(this).addClass("section-indicator-button--active");
                $('html, body').animate({ scrollTop: t.offset().top}, 500, 'swing');
            });



        });
        // /DOCUMENT READY BLOCK ///////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////


    </script>
</script>
<!-- /Services; Page; @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->