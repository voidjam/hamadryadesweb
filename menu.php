<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Menu Page ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script id="menu_content" type="text/template">
  <div class="menu_content">

      

      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
      <!--  -->
      <figure class="menu-background-color"></figure>
      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



      <!-- Texto hamadryades adjunto al logo -->
      <span class="logo_text">hamadryades</span>

      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
      <!-- Section buttons. -->
      <ol class="list-menu">
          <li class="list-item list-item-menu">
              <a class="button button-menu button-menu--about" onclick="showPage(page_section.about)" data-index="0">
                  <span class="first-letter">N</span><span>osotros</span>
              </a>
          </li>
          <li class="list-item list-item-menu">
              <a class="button button-menu button-menu--services" onclick="showPage(page_section.services)" data-index="1">
                  <span class="first-letter">S</span><span>ervicios</span>
              </a>
          </li>

          <li class="list-item list-item-menu">
              <a class="button button-menu button-menu--projects" onclick="showPage(page_section.projects)" data-index="2">
                  <span class="first-letter">P</span><span>royectos</span>
              </a>
          </li>
      </ol>
      <!-- /Section buttons. -->
      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
      <!-- GET IN TOUCH LINKS -->
      <div id="menu-link-contact" class="home-link-contact" onclick="showPage(page_section.contact)">
          <a>Contacta con Nosotro<span class="home-link-contact-last-letter">s</span></a>
      </div>
      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->




      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
      <!-- SECTION PAGE INFO -->
      <div class="page_section_info unselectable">
        <span>MENU</span>
      </div>
      <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  </div>
  </script>
<!-- /Menu Page +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->