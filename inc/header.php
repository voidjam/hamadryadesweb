<!DOCTYPE html>
<html lang="es">
	<head>
		<!--
		<?php
			//header("X-XSS-Protection: 1");
			//header("Accept-Language: ".$_SESSION['giu_language']); 
			//header('Content-type: text/html; charset=utf-8');
			//header('Cache-Control: cache');
			//header('Pragma: no-cache');
			//header('Content-Security-Policy: img-src *');
		?>-->
		
		<meta charset="utf-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta http-equiv="Cache-control" content="public">

		<meta http-equiv="Content-Security-Policy" content="default-src *;
		   img-src * 'self' data: https:; script-src 'self' 'unsafe-inline' 'unsafe-eval' *;
		   style-src  'self' 'unsafe-inline' *">

		<meta name="description" content="" />
		

		<!-- Title -->
		<title>hamadryades<?php if(isset($pageTitle)) echo " - ".$pageTitle; ?></title>
	
		<link rel="alternate" hreflang="es" href="http://www.hamadryades.es/es" />
		<link rel="alternate" hreflang="en" href="http://www.hamadryades.es/en" />


        <!-- KEYWORDS -->
        <meta name="Keywords" content="
        estudio de paisajismo, diseño de jardines, ejecución de jardines, agronomía,
        medio ambiente, paisajismo, jardinería,

        sostenibilidad, mnantenimiento diferenciado, huerto, jardines pequeños, escala territorial, paisajismo,
        ejecución de jardines, estudios de impacto, integración paisajística, tramitación ambiental, cambio climático,
        seguridad alimentaria, agricultura urbana, minoración de impactos, evaluación ambiental de planes,
        evaluación ambiental estratégica,  evaluación de impacto ambiental, Cities, Elsevier,

        susana díaz-palacios sisternes, francisco lafuente terceros, paisaje, paisajista, agronomía,
        arquitectura, diseño de jardines, ejecución de jardines, estudios de paisaje, proyectos agrarios,
        proyectos medioambientales, paisaje visual, impactos paisajísticos, procesos ecológicos, sinantropización,
        integración ambiental, hábitat,integración ambiental, soluciones personalizadas,

        integración paisajística, fachada marítima, línea del horizonte, puntos de observación, puerto, mosaicocultura,
        mantenimiento de jardines, restauración de jardines, jardín, zonas verdes, análisis botánico,
        sistemas de información geográfica, huerto, huerto ecológico,plantones,integración ecológica,
        vistas panorámicas, Peñón de Gibraltar, barreras cortavientos, confort climático, integración ambiental,
        ganado caprino, quesería artesanal, Comunidad de Madrid, territorio – proyecto, Universidad Politécnica de
        Madrid, UPM, LIDAR. Modelo digital de elevaciones, Comunidad Autónoma del País Vasco,

        hamadryades, contacto"/>


		<!-- CANONICAL: Indica qué versión del contenido duplicado es la buena. -->
		<link rel='canonical' href='http://hamadryades.com/'/>

		<!-- OPEN GRAPH -->
		<meta property="og:image" content="http://www.hamadryades.com/images/favicon/android-chrome-512x512.png" />
		<meta property="og:title" content="hamadryades" />
		<meta property="og:type" content="website" />
		<meta property='og:url' content='http://www.hamadryades.com/' />
		<meta property="og:description" content="Estudio de paisajismo, diseño y ejecución de jardines. Agronomía y Medio ambiente." />
		<meta property="og:site_name" content="Hamadryades" />
		<meta property="og:locale" content="es_ES" />
        <meta property="article:author" content="Ángel Blanco (umboweti.com)">


        <!-- Google -->
        <meta name="google-site-verification" content="verification_token">
        <!-- Google+ / Schema.org -->
        <link href="https://plus.google.com/u/0/101036897474557739729" rel="publisher">
        <meta itemprop="name" content="Hamadryades">
        <meta itemprop="description" content="Estudio de paisajismo, diseño y ejecución de jardines. Agronomía y Medio ambiente.">
        <meta itemprop="image" content="https://www.hamadryades.com/images/favicon/android-chrome-512x512.png">

		<!-- Twitter -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="http://www.hamadryades.com/" />
		<meta name="twitter:title" content="Hamadryades" />
		<meta name="twitter:description" content="Estudio de paisajismo, diseño y ejecución de jardines. Agronomía y Medio ambiente." />
		<meta name="twitter:image" content="https://www.hamadryades.com/images/favicon/android-chrome-512x512.png" />
		
		<!-- Facebook -->
		<meta property="fb:app_id" content="123456789" />


        <!-- Favicons -->
        <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
        <link rel="manifest" href="images/favicon/manifest.json">
        <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="images/favicon/favicon.ico">
        <meta name="msapplication-config" content="images/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">


        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
        <!-- Font Awesome -->
	    <script src="https://use.fontawesome.com/8184c088bf.js"></script>

		<!-- CSS -->
		<!-- JQUERY CSS -->
		<link rel="stylesheet" href="<?php echo $rootpath; ?>css/jquery-ui.css">
		<!-- BOOTSTRAP CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<!-- MY CSS -->
		<link rel="stylesheet" href="<?php echo $rootpath; ?>css/estilo.css" />
        <!-- FluidBox -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fluidbox/2.0.5/css/fluidbox.min.css" integrity="sha256-5oR7lL1eUeRKAkRd76G2tJ1pNrqsKIOmJZrzOIpq8Gc=" crossorigin="anonymous"/> -->
        <!-- Fancybox -->
        <link rel="stylesheet" href="<?php echo $rootpath; ?>css/jquery.fancybox.min.css" />



        <!-- JS -->
		<!-- JQUERY JS-->
		<script src="<?php echo $rootpath; ?>js/jquery-3.2.1.min.js"></script>
		<script src="<?php echo $rootpath; ?>js/jquery-ui.js"></script>
		<!-- BOOTSTRAP JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		<!--CDN links for TweenLite, CSSPlugin, and EasePack-->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/CSSPlugin.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
		<!-- JS LIBS -->
		<script src="<?php echo $rootpath; ?>libs/opentype.js"></script>
		<!-- MY JS -->
		<script type="text/javascript" src="<?php echo $rootpath; ?>js/functions.js"></script>
		<!-- RIPPLES EFFECT -->
		<script src="<?php echo $rootpath; ?>libs/jquery.ripples-min.js"></script>
        <!-- FLUIDBOX -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fluidbox/2.0.5/js/jquery.fluidbox.min.js" integrity="sha256-AVBLVWwvNE9uzNP36wynCSm3JIIkjvjXsj/Ol30JzGM=" crossorigin="anonymous"></script> -->
        <!-- FANCYBOX -->
        <script src="<?php echo $rootpath; ?>js/jquery.fancybox.min.js"></script>

        <!-- AUTOSIZE TEXTAREA JS -->
        <script src="<?php echo $rootpath; ?>js/autosize.min.js"></script>


        <!-- Google Maps API -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdLGA9_p6BBOtL8PO37nNYxNlk5Z9BEMc" async defer></script>

        <!-- Google Analytics Code -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-68038945-1', 'auto');
            ga('send', 'pageview');
        </script>


	</head><body>