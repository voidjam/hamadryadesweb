<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Home Page Content ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script id="home_content" type="text/template">
    <div class="home_content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- VINES -->
        <!-- Here we will put the canvas fot the vines animation -->
        <figure id="vines-figure"></figure>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- INTRO TEXT -->
        <div class="page-intro container-fluid text-center" id="page-intro">
            <!-- Texto hamadryades adjunto al logo -->
            <span class="logo_text d-md-none" style="font-weight: 300; font-size:2rem;">hamadryades</span>
            <br>
            <h2 class="page-intro-slogan">Estudio de paisajismo, diseño y ejecución de jardines.<br>Agronomía y Medio ambiente.</h2>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION BUTTONS -->
        <div class="section-button-container">
        <!--
          BOOTSTRAP 3 GRID BREAKPOINTS:
              .col-xs-  Extra small   <768px    Phones
              .col-sm-  Small         ≥768px    Tablets
              .col-md-  Medium        ≥992px    Desktop
              .col-lg-  Large         ≥1200px   Desktop
        -->

        <!-- 1 ROW: NO BOOTSTRAPT -->
        <div class="buttons-row">
            <!-- ABOUT BUTTON -->
            <div class="buttons-col">
                <div class="section-button about-button animated-border hoverable" onclick="showPage(page_section.about)">
                    <div class="anim"></div><span>Nosotros</span></div>
            </div>
            <!-- SERVICES BUTTON -->
            <div class="buttons-col">
            <div class="section-button services-button animated-border hoverable" onclick="showPage(page_section.services)">
              <div class="anim"></div>Servicios</div>
            </div>
            <!-- PROJECTS BUTTON -->
            <div class="buttons-col">
            <div class="section-button proyects-button animated-border hoverable" onclick="showPage(page_section.projects)">
              <div class="anim"></div><span>Proyectos</span></div>
            </div>
        </div>
        <!-- 1 ROW: NO BOOTSTRAPT -->
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- GET IN TOUCH LINK -->
        <div class="home-link-contact" id="home-link-contact" onclick="showPage(page_section.contact)">
            <a>Contacta con Nosotro<span class="home-link-contact-last-letter">s</span></a>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- MEDIA LINKS -->
        <div class="media-link-container">
            <a href="https://twitter.com/hamadryades_map" class="icon icon-cube twitter" target="_blank">twitter</a>
            <a href="https://www.pinterest.es/hamadryades_map/" class="icon icon-cube pinterest" target="_blank">pinterest</a>
            <a href="https://plus.google.com/u/0/101036897474557739729" class="icon icon-cube googleplus" target="_blank">google+</a>
        </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- COPPYRIGHT -->
        <div class="d-md-none copyright-mob-home text-center"><span>©hamadryades 2017</span><span> | </span><span>info@hmap.es</span></div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    </div><!-- /home-content -->

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span>HOME</span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- LEGAL DOCS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="legal-docs unselectable">
        <a href="./docs/aviso_legal.pdf" target="_blank" title="Aviso Legal">LEGAL</a>
        <span> | </span>
        <a href="./docs/politica_privacidad.pdf" target="_blank" title="Política de Privacidad">PRIVACIDAD</a>
        <span> | </span>
        <a href="http://umboweti.com" target="_blank" title="Ir a la Página de los Creadores">UMBOWETI</a>
        <span> | </span>
        <a style="cursor: default; pointer-events: none">©hamadryades 2017</a>
        <span style="vertical-align: middle"> • </span>
        <a href="mailto:info@hmap.es" target="_top" title="Email">info@hmap.es</a>
    </div>
    <!-- /LEGAL DOCS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->




    <script type="text/javascript" visivility="hidden">

        // ON DOCUMENT READY.
        $(function() {

            //////////////////////////////////////////////////////////////////////////////////////////
            // RIPPLE CLICK EVENT FOT THE SECTION BUTTONS. ///////////////////////////////////////////
            $(".section-button").click(function (e) {

                // Remove any old one
                $(".ripple").remove();

                // Setup
                var posX = $(this).offset().left,
                    posY = $(this).offset().top,
                    buttonWidth = $(this).width(),
                    buttonHeight = $(this).height();

                // Add the element
                $(this).prepend("<span class='ripple'></span>");


                // Make it round!
                if (buttonWidth >= buttonHeight) {
                    buttonHeight = buttonWidth;
                } else {
                    buttonWidth = buttonHeight;
                }

                // Get the center of the element
                var x = e.pageX - posX - buttonWidth / 2;
                var y = e.pageY - posY - buttonHeight / 2;


                // Add the ripples CSS and start the animation
                $(".ripple").css({
                    width: buttonWidth,
                    height: buttonHeight,
                    top: y + 'px',
                    left: x + 'px'
                }).addClass("rippleEffect");
            });
            //////////////////////////////////////////////////////////////////////////////////////////

        });

</script>
<!-- /Home Page Content  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->