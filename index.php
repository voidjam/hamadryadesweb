<?php

    // Start session.
    session_start(); 
    
    // Define root path and page name.
    $rootpath = './';

    if(isset($_GET['url'])) {
      $_SESSION["section"] = $_GET['url'];
    } else {
      $_SESSION["section"] = 'home';
    }

    // Including headers.
    include ($rootpath.'inc/header.php');

    // Loading scripts containing HTML code for each section.
    include ($rootpath.'menu.php');
    include ($rootpath.'home.php');
    include ($rootpath.'contact.php');
    include ($rootpath.'about.php');
    include ($rootpath.'projects.php');
    include ($rootpath.'services.php');
?>

    <!-- This div allows us to isolate the water drops effect    -->
    <!-- in one single div, letting us change the page_container -->
    <!-- content without affecting the drop effect.              -->
    <div class="background_layer"></div>


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- LOGO -->
    <div class="logo-container" onclick="showPage(page_section.home)">
      <div class="breathing-div d-none d-md-block">
        <!-- Include SVG logo code -->
        <?php include($rootpath.'images/start_logo.svg'); ?>
      </div><!-- /breathing-div -->
        <div class="breathing-div d-none d-sm-block d-md-none">
        <!-- Include SVG logo code -->
        <?php include($rootpath.'images/start_logo_sm.svg'); ?>
      </div><!-- /breathing-div -->
        <div class="breathing-div d-block d-sm-none">
            <!-- Include SVG logo code -->
            <?php include($rootpath.'images/start_logo_xs.svg'); ?>
        </div><!-- /breathing-div -->
    </div><!-- /logo-container -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- MENU BUTTON -->
    <button id="openMenu" class="hamburger hamburger--squeeze" type="button" alt="MENU">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
        <!-- <span class="hamburger-label">Menu</span> -->
    </button>
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->






    <!-- This div will be the web page container: Closed on footer. -->
    <div class="page_container">

    <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- Page Loader Content ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="loader_content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- BLACK LAYER FOR BACKGROUND OPACITY -->
        <div class="black-layer"></div>
        <script>
          // Variable to set black-layer opacity. 0 layer is hidden.
          var $blackLayerAA = 0;
        </script>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- TITLE -->
        <div id="pageTitle" class="pageTitle container-fluid text-center">
          <h1>Hamadryades</h1>
          </div>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div><!-- /Page Loader Content +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- This script is the entrance to the page, any request to the server is served by index.php -->
    <script type="text/javascript">


        // Boolean to know if page is being load:
        var firstLoad = true;

        // This variable will store the current section. On the httprequest will be set
        // by $_SESSION["section"], and afterwards by the funcitons showHome(), showContact(), etc.
        var section;

        var tl;


        // Screen Size Variables:
        var breakpoint = {value:String()}, // The variable breakpoint will store the kind of the current device screen.
            // The rest of the variables are values needed to properly show the page that
            // depend on the current device screen size.
            logo_size = String(),
            logo_container = {top:String(), x:String()},
            page_intro = {top:String(), x:String(), onComplete:new Function(), onCompleteParams:[]},
            section_button_container = {top:String(), autoAlpha:Number()},
            background_layer = { showRipples:String()}, // showRipples = 'show || hide' to show water drops background effect.
            menu_opened = Boolean(); // We will store if the menu is currently open or closed.




        /////////////////////////////////////////////////////////////////////////////////////////
        // SET MEDIAQUERY AND LISTENERS TO DETECT WINDOW SIZE CHANGES ///////////////////////////
        // Array to store all of our window.matchMedia() queries first,
        // then use a for loop to invoke a single function that handles all of the queries.
        var mqls = [ // list of window.matchMedia() queries
                      window.matchMedia("(max-width: 575px)"),    // XS - Portrait Smartphones
                      window.matchMedia("(min-width: 576px)"),    // SM - Landspace Smartphones / Tablet
                      window.matchMedia("(min-width: 768px)"),    // MD - Tablets
                      window.matchMedia("(min-width: 992px)"),    // LG - Desktop
                      window.matchMedia("(max-width: 1200px)")];  // XL - Large
        // var mqls = [ // list of window.matchMedia() queries
        //               window.matchMedia("(min-width: 769px) and (max-width: 1201px)"),    // Width above 768px and below 1200px .
        //               window.matchMedia("(min-width: 1202px)"),                           // Width 1200px or above.
        //               window.matchMedia("(max-width: 768px)")];                           // Width 768px  or below.
        /**
         * This method is the mediaquery listener that will listen to  the event thrown when screen width changes.
         * The queries are triggered every time mql.matches changes value (true or false), so every time a threshold
         * is passed, this function triggers twice. Checking its mql.media value we can make that the code is
         * executed only once, for the one that has currently a mql.matches == true.
         */
        function mediaqueryresponse(mql) {

            // XS
            if (mqls[0].matches && mql.media.includes("(max-width: 575px)")) {
              breakpoint = 'xs';
              mobileScreen();
            }

            // SM
            if (mqls[1].matches && mql.media.includes("(min-width: 576px)")) {
              breakpoint = 'sm';
              mobileScreen();
            }

            // MD
            if (mqls[2].matches && mql.media.includes("(min-width: 768px)")) {
              breakpoint = 'md';
              laptopScreen();
            }

            // LG
            if (mqls[3].matches && mql.media.includes("(min-width: 992px)")) {
              breakpoint = 'lg';
              laptopScreen();
            }

            // XL
            if (mqls[4].matches && mql.media.includes("(min-width: 1200px)")) {
              breakpoint = 'xl';
              desktopScreen();
            }

        }

        // Here we will loop the querys to execute mediaqueryresponse() for each
        // one of them and execute the function corresponding the query that match
        // the current screen size. We will also add hte same listener to all the
        // queries so mediaqueryresponse() will be also called on any resize events,
        // when any of the queries changes its state.
        for (var i=0; i<mqls.length; i++) {
          // Call handler function explicitly three times when the page first loads,
          // one time each to deal with each query that may be matched when the page first loads
          mediaqueryresponse(mqls[i]);
          // Call handler function whenever the media query is triggered
          mqls[i].addListener(mediaqueryresponse)
        }
        /////////////////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////////////////////////////////////////////////
        // FUNCTIONS TRIGGERED TO MANAGE SCREEN WIDTH CHANGES. ///////////////////////////////////
        function desktopScreen()  {

            if ($('#myCanvas').length) {
              $("#myCanvas").remove();
              showVinesAnimation("Hamadryades", 240);
              TweenLite.to($("#myCanvas"), 0.6, { ease: Power4.easeOut, top:"57%" });  // TODO: No funciona.

            }


            //alert("desktop");
            logo_size = "0.4";
            logo_container.top = "18%";
            logo_container.x = "-51%";
            page_intro.top = "66%";
            page_intro.onComplete = showVinesAnimation;
            page_intro.onCompleteParams = ['hamadryades'];
            section_button_container.top ="74%";
            section_button_container.autoAlpha = 1 ;

            background_layer.showRipples = 'hide';
        }
        function laptopScreen () {

            /*
            TweenLite.to($(".logo-container") , 1,  { ease: Power2.easeOut, scale: 0.4, top:"18%", x:"-50%"});
            TweenLite.to($(".black-layer-svg"), 0.6, { ease: Power4.easeOut, autoAlpha:0 });
            TweenLite.to($(".pageTitle"), 0.5, { top: 0, autoAlpha:0})
            TweenLite.to($(".page-intro"), 0.6, {ease: Back.easeOut.config(1.7), top:"66%"});
            TweenLite.to($(".section-button-container"), 0.5, { top: "74%", autoAlpha:1});

            $("#myCanvas").remove();
            showVinesAnimation("hamadryades");
            TweenLite.to($("#myCanvas"), 0.6, { ease: Power4.easeOut, autoAlpha:1 });

            $('.background_layer').ripples('show');
            */
            //alert("laptop");
            logo_size = "0.4";
            logo_container.top = "18%";
            logo_container.x = "-51%";
            page_intro.top = "64%";
            page_intro.onComplete = showVinesAnimation;
            page_intro.onCompleteParams = ['hamadryades'];
            section_button_container.top ="72%";
            section_button_container.autoAlpha = 1 ;

            background_layer.showRipples = 'hide';

          }
        function mobileScreen ()  {

            /*
            TweenLite.to($(".logo-container"), 1,  { ease: Power2.easeOut, scale: 0.6, top:"33%", x:"-50%"});
            TweenLite.to($(".page-intro"), 0.6, {ease: Back.easeOut.config(1.7), top:"52%"});
            TweenLite.to($(".pageTitle"), 0.5, { top: 0, autoAlpha:1});
            TweenLite.to($(".section-button-container"), 0.5, { top: "66%", autoAlpha:1});

            if ($('#myCanvas').length) {
              TweenLite.to($("#myCanvas"), 0.6, { ease: Power4.easeOut, autoAlpha:0 });
            }

            $('.background_layer').ripples('hide');
            */
            //alert("mobile");
            logo_size = "0.6";
            logo_container.top = "23%";
            logo_container.x = "-50%";
            page_intro.top = "45%";
            page_intro.onComplete = null;
            page_intro.onCompleteParams = null;
            section_button_container.top ="60%";
            section_button_container.autoAlpha = 1 ;

            background_layer.showRipples = 'hide';


          }

        //////////////////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////////////////////////////////////////////////
        // ON POP STATE //////////////////////////////////////////////////////////////////////////
        window.onpopstate = function(event) {
            //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
            showPage(event.state.page);
        };
        // /ON POP STATE /////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////////////////////////////////////////////////
        // CLOSE MENU ON ESC KEY PRESS ///////////////////////////////////////////////////////////
        // Close menu if open when pressing esc key on keyboard.
        $(document).keyup(function(e) {
            if (e.keyCode === 27 && menu_opened) { // escape key maps to keycode `27`
                $("button#openMenu").click();
            }
        });
        //////////////////////////////////////////////////////////////////////////////////////////
        // /CLOSE MENU ON ESC KEY PRESS //////////////////////////////////////////////////////////





        //////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY FUNCTION ///////////////////////////////////////////////////////////////
        $(document).ready(function() {

            section = "<?php echo $_SESSION["section"]?>";

            ///////////////////////////////////////////////////////
            // ADDING WATER RIPPLE EFFECT /////////////////////////
            // Start ripple pluggin.
//              try {
//
//                  $('.background_layer').ripples({
//                      resolution: 1024,   // The width and height of the WebGL texture to render to. The larger this value, the smoother the rendering and the slower the ripples will propagate.
//                      dropRadius: 10,     // The size (in pixels) of the drop that results by clicking or moving the mouse over the canvas.
//                      perturbance: 0.01,  // Basically the amount of refraction caused by a ripple. 0 means there is no refraction.
//                      interactive: false  // Whether mouse clicks and mouse movement triggers the effect.
//                  });
//              } catch (e) {
//                $('.error').show().text(e);
//              }
//
//              // Adding automatic drops only if window is on focus.
//              var myInterval;
//              var interval_delay = 4000;
//              var is_interval_running = false; //Optional
//
//              $(window).focus(function () {
//                  clearInterval(myInterval); // Clearing interval if for some reason it has not been cleared yet
//                  if  (!is_interval_running) //Optional
//                      myInterval = setInterval(set_drops, interval_delay);
//              }).blur(function () {
//                  clearInterval(myInterval); // Clearing interval on window blur
//                  is_interval_running = false; //Optional
//              });
//
//
//              set_drops = function () {
//                  is_interval_running = true; //Optional
//                  // Code running while window is in focus
//                  var $el = $('.background_layer');
//                  var x = Math.random() * $el.outerWidth();
//                  var y = Math.random() * $el.outerHeight();
//                  var dropRadius = 10;
//                  var strength = 0.02 + Math.random() * 0.02;
//
//                  $el.ripples('drop', x, y, dropRadius, strength);
//              };
//
//              myInterval = setInterval(set_drops, interval_delay);

            // /ADDING WATER RIPPLE EFFECT ////////////////////////
            ///////////////////////////////////////////////////////


            // Create a timeline
            tl = new TimelineLite();
            // Hide the dark layer from the loading animation and show the corresponding section.
            tl.to($(".black-layer"), 0.6, { ease: Power4.easeOut, autoAlpha: $blackLayerAA, onComplete:showPage, onCompleteParams:[section, true]}, 2);


        });
        // /DOCUMENT READY FUNCTION //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////


        /**
         * This function is called whenever we want to access any of the page sections. The functions
         * showHome(), showServices(), etc. shall not be called directly but throw showPage.
         *
         * @param section Indicates the section we want to load.
         * @param newLoad Indicates if we are loading from a new http request or just changing section.show
         * @param projFilter Only for showProjects(), if any of the services filter must be preselected.
         */
        function showPage(section, newLoad = false, projFilter = false) {

            window.section = section;
            window.history.pushState({page: window.section}, "", "/" + window.section);

            menu_opened = false;

            $(".page_container").trigger(custom_events.pageChangeEvent);

            if (section === page_section.home) {
                showHome(newLoad);
            } else if (section === page_section.contact) {
                showContact(newLoad);
            } else if (section === page_section.about) {
                showAbout(newLoad);
            } else if (section === page_section.projects)    {
                showProjects(newLoad, projFilter);
            } else if (section === page_section.services)    {
                showServices(newLoad);
            }
        }

        function showHome(newLoad = false) {

            // If page has just loaded we transform the logo:
            if (newLoad === true) {
                // CHARGE HOME CONTENT:
                $(".page_container").empty();
                $(".page_container").html($("#home_content").html());
                // Set the logo without loading animation:
                $( ".water-effect" ).addClass( "animOff" );

                tl.to($(".logo-container"), 1,  { ease: Power2.easeOut, scale: logo_size, top:logo_container.top, x:logo_container.x}, 2.6);
                // Set on of off the water drops effect on the background.
                //$('.background_layer').ripples(background_layer.showRipples);
                // Attach a handler to an event for the elements. The handler is executed at most once per element per event type.
                // In this case showvinesAnimation() will be triggered after the page-intro has finished its opacity animation.
                tl.to($(".page-intro"), 0.6, {ease: Back.easeOut.config(1.7), top:page_intro.top , autoAlpha:1, onComplete:page_intro.onComplete, onCompleteParams:page_intro.onCompleteParams}, "-=1")
                // Show section buttons:
                .to($(".section-button-container"), 2, {top: section_button_container.top, autoAlpha:section_button_container.autoAlpha}, "-=1.6"); // TODO: width: $('#myCanvas').width() No funciona.

                ///////////////////////////////////////////////////////
                // SHOW "GET IN TOUCH" & "JAMBOO" & SOCIAL LINKS. /////
                TweenMax.to($('.home-link-contact'),0.4 , {opacity : 1, delay: 0.6 });
                // TweenLite.to($(".home-link-jamboo"), newLoad ? 0.6 : 0.4 , {autoAlpha: 1, delay: newLoad ? 0.6 : 0 });
                TweenMax.to($('.media-link-container'),0.6 , {opacity: 1, delay: 0.6 });
                ///////////////////////////////////////////////////////

            } else {
                // CHARGE HOME CONTENT:
                $('.page_container').children().fadeOut(150).promise().done(function() {

                    $(".page_container").empty();
                    $(".page_container").html($("#home_content").html());
                    // Set the logo without loading animation:
                    $(".water-effect").addClass("animOff");

                    tl = new TimelineLite();

                    $(".logo-container").fadeIn(400); // This is necessary when coming from projects, or services or about. logo-container has been fadeOut() and TweenMax does not make it appear.
                    tl.to($(".logo-container"), 0.4,  { ease: Power2.easeOut, scale: logo_size, top:logo_container.top, x:logo_container.x});
                    // Set on of off the water drops effect on the background.
                    //$('.background_layer').ripples(background_layer.showRipples);
                    // Attach a handler to an event for the elements. The handler is executed at most once per element per event type.
                    // In this case showvinesAnimation() will be triggered after the page-intro has finished its opacity animation.
                    tl.fromTo($(".page-intro"), 0.4,{top:(page_intro.top+30), autoAlpha:0}, {top:page_intro.top , autoAlpha:1, onComplete:page_intro.onComplete, onCompleteParams:page_intro.onCompleteParams})
                    // Show section buttons:
                        .fromTo($(".section-button-container"), 0.4, {top:(section_button_container.top+30), autoAlpha:0}, {top: section_button_container.top, autoAlpha:section_button_container.autoAlpha}, "-=0.4"); // TODO: width: $('#myCanvas').width() No funciona.

                    ///////////////////////////////////////////////////////
                    // SHOW "GET IN TOUCH" & "JAMBOO" & SOCIAL LINKS. /////
                    TweenMax.to($('.home-link-contact'),0.4, {autoAlpha : 1, delay: 0.4});
                    // TweenLite.to($(".home-link-jamboo"), newLoad ? 0.6 : 0.4 , {autoAlpha: 1, delay: newLoad ? 0.6 : 0 });
                    TweenMax.to($('.media-link-container'),0.4 , {opacity: 1, delay:  0.4});
                    ///////////////////////////////////////////////////////
                });

            }

            // Show the MENU BUTTON (#openMenu)
            TweenMax.set($('#openMenu'), {autoAlpha:1, onComplete:setMenuButton});

        }

        function showContact(newLoad = false)  {

            // If newLoad we will take care of loading animation (black_layer and logo).
            if (breakpoint != 'xs' && breakpoint != 'sm') { // we don't want the logo in mobile version
                if (newLoad === true) {
                    TweenMax.to($(".logo-container"), 1, {
                        ease: Power2.easeOut,
                        scale: logo_size,
                        top: logo_container.top,
                        x: logo_container.x
                    }, 2.6);
                    // Else we just make sure the logo is on its proper position.
                } else {
                    TweenMax.set($(".logo-container"), {
                        scale: logo_size,
                        opacity: 1,
                        top: logo_container.top,
                        x: logo_container.x
                    }, 0.15);
                }
            } else {
                // Fading out logo-container.
                $(".page_container").children().add($(".logo-container")).fadeOut(300);
            }

          $('.page_container').children().fadeOut(150).promise().done(function() {

              // Empty .page_container and load new content.
              $(".page_container").empty();
              $(".page_container").html($("#contact_content").html());

              // Show the contact_content first child (#openMenu) with an fade in effect:
              TweenMax.to($('#openMenu'), 0.4, {autoAlpha:1});
              // Show the rest of the children with a drop from down and fadeInwe effect and set Menu Button functionallity.
              TweenMax.from($('.contact_content').children(), 0.4, {y:"+=30", opacity:0, onComplete:setMenuButton});

              // Hide/Show ripple background effect:
              //$('.background_layer').ripples(background_layer.showRipples);

          });

        }

        function showAbout (newLoad = false) {

            // Fading out all page_container children as well as the logo-container.
            $(".page_container").children().add($(".logo-container")).fadeOut(300).promise().done(function() {

                $(".page_container").empty();
                $(".page_container").html($("#about_content").html());

                // Show the #openMenu button with an fade in effect:
                TweenMax.to($('#openMenu'), 0.3, {autoAlpha:1});
                // Show the rest of the children with a drop from down and fadeInwe effect and set Menu Button functionallity.
                TweenMax.from($('.page_container').children(), 0.3, {y:"+=30", opacity:0, onComplete:setMenuButton});
                // Hide/Show ripple background effect:
                //$('.background_layer').ripples(background_layer.showRipples);

            });
        }

        function showProjects (newLoad = false, service = false)  {

            // Fading out all page_container children as well as the logo-container.
            $(".page_container").children().add($(".logo-container")).fadeOut(300).promise().done(function() {
                // Scroll to window top not to remember scroll position when coming from services.
                window.scrollTo(0,0);

                $(".page_container").empty();
                $(".page_container").html($("#projects_content").html());

                // Show the #openMenu button with an fade in effect:
                TweenMax.to($('#openMenu'), 0.3, {autoAlpha:1});
                // Show the rest of the children with a drop from down and fadeInwe effect and set Menu Button functionallity.
                TweenMax.from($('.page_container').children(), 0.3, {y:"+=30", opacity:0, onComplete:function() {
                    setMenuButton();
                    // Check the services checkbox if coming from services links:
                    if (service !== false) $("[data-serv-chk="+service+"]").click();
                }});
                // Hide/Show ripple background effect:
                //$('.background_layer').ripples(background_layer.showRipples);
            });
        }

        function showServices(newLoad = false) {

            // Fading out all page_container children as well as the logo-container.
            $(".page_container").children().add($(".logo-container")).fadeOut(300).promise().done(function() {

                $(".page_container").empty();
                $(".page_container").html($("#services_content").html());

                // Show the #openMenu button with an fade in effect:
                TweenMax.to($('#openMenu'), 0.3, {autoAlpha:1});
                // Show the rest of the children with a drop from down and fadeInwe effect and set Menu Button functionallity.
                TweenMax.from($('.page_container').children(), 0.3, {y:"+=30", opacity:0, onComplete:setMenuButton});
                // Hide/Show ripple background effect:
                //$('.background_layer').ripples(background_layer.showRipples);

            });
        }


        function showMenu() {
            menu_opened = true;
            // Set the logo without loading animation.
            $( ".water-effect" ).addClass( "animOff" );
            // Make sure the logo is shown when we open the menu from projects, about or services sections.
            //$(".logo-container").fadeIn(0);
            TweenLite.set($(".logo-container"), {clearProps:"all"});
            TweenLite.to($(".logo-container"), 0.4,  { ease: Power2.easeOut, scale: logo_size, autoAlpha:1, top:logo_container.top, x:logo_container.x}, 0.2);

            $('.page_container').children().fadeOut(200).promise().done(function() {

                $(".page_container").append($("#menu_content").html());
                $(".button-menu--" + section).addClass("button-menu--active");

                $(".menu_content").visible();
                $(".button-menu").css('opacity'); // This is necessary for the transition animation to be run (JS Bug).
                $(".button-menu").css('opacity', '1');

                $("#menu-link-contact").css('opacity', '1');

                // Show the rest of the children with a drop from down and fadeInwe effect:
                TweenMax.from($('.page_section_info'), 0.4, {y:"+=30", opacity:0});
            });

        }

        function hideMenu() {
            menu_opened = false;
            // Fade out all menu child elements and the .logo-container (which is not inside .menu_content
            // but inside body element). In case we were visualizing the contact section we shall keep the
            // .logo-container where it is.
            let $childsToFadeOut = $('.menu_content').children();

            // We will also fade out the main logo except for home and contact sections on desktop screens,
            // and for home section on mobile screens.
            if(section !== page_section.contact && section !== page_section.home
                || ((breakpoint === 'xs' || breakpoint === 'sm') && section !== page_section.home))  {
                $childsToFadeOut = $('.menu_content').children().add(".logo-container");
            }

            $childsToFadeOut.fadeOut(200).promise().done(function() {
                $(".menu_content").remove();
                $('.page_container').children().remove('script').fadeIn(300);
            });
        }



        ///////////////////////////////////////////////
        // CLEAR SESSION STORAGE WHEN LEAVING THE PAGE:
        $(window).bind('beforeunload',function(){
            sessionStorage.clear()
        });
        ///////////////////////////////////////////////


    </script>

    <script type="text/javascript"> // RELOADS WEBPAGE WHEN MOBILE ORIENTATION CHANGES
        window.onorientationchange = function() {
            var orientation = window.orientation;
            switch(orientation) {
                case 0:
                case 90:
                case -90: window.location.reload();
                    break; }
        };
        </script>

    <?php
        // Including footer.
        include ($rootpath.'inc/footer.php');
    ?>