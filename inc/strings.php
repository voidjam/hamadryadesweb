<?php
/**
 * User: marcos
 * Date: 6/16/17
 * Time: 7:05 PM
 */

$projects = array(

    "project0" => array (
        "project_title"                 => "PATIO INTERIOR",
        "project_description"           => "Diseño y ajardinamiento de patio interior de viviendas",
        "project_addinfo_location"      => "MÁLAGA",
        "project_addinfo_text"          => "Diseño y ejecución de uno de los tres patios interiores de un bloque de viviendas rehabilitadas ubicadas en el centro de Málaga. El tipo de jardín propuesto es un patio ajardinado de casi 180m2 conformado por áreas de circulación y paseo, una fuente de agua y zonas verdes de distintas características. Dado que es un espacio a disfrutar también desde las diferentes alturas del edificio (con diferentes ángulos de incidencia visual), se han buscado y definido diferentes texturas y colores de las plantas que tapizan las mismas para aportar la mayor riqueza visual posible, gracias también a la relación compositiva planteada de las líneas del jardín con las líneas de fachada.",
        "project_image"                 => "images/projects/project1_patio_interior.jpg",
        "project_image_2"                 => "images/projects/project1_patio_interior_01.jpg",
        "project_image_2_opt"                 => "images/projects/project1_patio_interior_01_opt.jpg",
        "project_serv"                  => "paisajismo",
        "project_alt"                   => "Agapanto, Málaga",
        "project_hover"                 => "Patio interior ajardinado, Málaga"
    ),

    "project1" => array(
        "project_title"                 => "Quesería Artesanal",
        "project_description"           => "Explotación de ganado caprino y quesería artesanal en Collado Mediano",
        "project_addinfo_location"      => "Madrid",
        "project_addinfo_text"          => "Partimos de la realización de un estudio exhaustivo para establecer los procedimientos de integración ambiental más adecuados en aras a ubicar en una finca ubicada en  el municipio de Collado Mediano, las naves e instalaciones necesarias para el desarrollo de una explotación de ganado caprino en régimen semi-extensivo de 100 hembras reproductoras y quesería artesanal. El proyecto determina y justifica todos los parámetros constructivos que permitan mantener las condiciones de bienestar animal, técnico-sanitarias y de seguridad necesarias en dicho espacio. El objetivo ha sido integrar el proyecto de edificación en el territorio, traducir su potencialidad e identidad para que el par territorio - proyecto  convivan en armonía.",
        "project_image"                 => "images/projects/project2_queseria_artesanal.jpg",
        "project_image_2"                 => "images/projects/project2_queseria_artesanal_01.jpg",
        "project_image_2_opt"                 => "images/projects/project2_queseria_artesanal_01_opt.jpg",
        "project_serv"                  => "agro",
        "project_alt"                   => "Ganado caprino, Sierra de Guadarrama",
        "project_hover"                 => "Ganado caprino, Sierra de Guadarrama"

    ),

    "project2" => array(
        "project_title"                 => "Agricultura Urbana",
        "project_description"           => "Huerto ecológico de interior",
        "project_addinfo_location"      => "Madrid",
        "project_addinfo_text"          => "Diseño y creación de un huerto ecológico ubicado en la cocina de un domicilio particular. En una dimensión de algo más de medio m2, el proyecto comprende un total de 25 maceteros sobre un trasdosado de pladur que incluyen red de riego automatizada y drenaje. Las especies plantadas fueron plantones de tomate, pimiento, remolacha, rabanito, lechuga baby leaf, flores comestibles, plantas aromáticas  y fresas. El proyecto incluye una serie de recomendaciones de mantenimiento general, además de específicas de cada especie cultivada.",
        "project_image"                 => "images/projects/project3_huerto_interior.jpg",
        "project_image_2"                 => "images/projects/project3_huerto_interior_01.jpg",
        "project_image_2_opt"                 => "images/projects/project3_huerto_interior_01_opt.jpg",
        "project_serv"                  => "paisajismo",
        "project_alt"                   => "Huerto vertical interior, Madrid",
        "project_hover"                 => "Huerto vertical interior, Madrid"
    ),

    "project3" => array(
        "project_title"                 => "Estudio de paisaje",
        "project_description"           => "Modificación APE Ermita del Santo",
        "project_addinfo_location"      => "Madrid",
        "project_addinfo_text"          => "Estudio de paisaje que analiza los efectos e impactos visuales que provocaría en la escena urbana,  una torre de 28 plantas propuesta en la Modificación del PGOU para esa zona de Madrid en el año 2016. El estudio contempla el análisis de las unidades de paisaje en el entorno del ámbito, la caracterización y valoración del mismo, una evaluación profunda de la afección a la escena urbana y la fijación de los objetivos de calidad paisajística.",
        "project_image"                 => "images/projects/project4_ermita_santo.jpg",
        "project_image_2"                 => "images/projects/project4_ermita_santo_01.jpg",
        "project_image_2_opt"                 => "images/projects/project4_ermita_santo_01_opt.jpg",
        "project_serv"                  => "paisaje",
        "project_alt"                   => "Estudio de paisaje Madrid",
        "project_hover"                 => "Estudio de paisaje Madrid"
    ),

    "project4" => array(
        "project_title"                 => "Estudio de paisaje",
        "project_description"           => "Ampliación de estudio paisajístico en Villaluenga del Rosario – Benaocaz",
        "project_addinfo_location"      => "Cadiz",
        "project_addinfo_text"          => "Ampliación del estudio de impacto visual que, en 2013, se realizó de las cinco alternativas de trazado propuestas para el desarrollo del proyecto de la Línea de Media Tensión desde Villaluenga del Rosario a Benaocaz, en Cádiz. En el estudio previo, también realizado por nosotros,  se consideraron las alternativas que provocaran un menor impacto paisajístico. Este nuevo estudio aborda las opciones identificadas como aptas en el estudio anterior, así como dos nuevas alternativas propuestas por la Consejería de Medio Ambiente y Ordenación del Territorio de la Junta de Andalucía.",
        "project_image"                 => "images/projects/project5_ronda_villaluenga.jpg",
        "project_image_2"                 => "images/projects/project5_ronda_villaluenga_01.jpg",
        "project_image_2_opt"                 => "images/projects/project5_ronda_villaluenga_01_opt.jpg",
        "project_serv"                  => "paisaje",
        "project_alt"                   => "Manuel Moreno García. BIOGEOS Estudios Ambientales, Consejería de Medio Ambiente, Junta de Andalucía",
        "project_hover"                 => "Manuel Moreno García. BIOGEOS Estudios Ambientales, Consejería de Medio Ambiente, Junta de Andalucía"
    ),

    "project5" => array(
        "project_title"                 => "Estudio de paisaje",
        "project_description"           => "Estudio de impacto e integración paisajística del puerto de Foz",
        "project_addinfo_location"      => "Lugo",
        "project_addinfo_text"          => "Estudio y análisis de los efectos e impactos que el Plan Especial de Ordenación del Puerto de Foz de 2015  provocaría en el paisaje y desarrollo de las medidas de integración paisajística. La reordenación del frente marítimo implicaría una modificación de la imagen actual de la fachada marítima debido a las nuevas edificaciones propuestas en dicho Plan, cuyo objetivo es reordenar las actividades portuarias y revitalizar la navegación recreativa y deportiva. El estudio aborda, entre otros puntos, el análisis desde distintos puntos de observación, de la ruptura de la línea de horizonte o la relación escalar y compositiva entre los perfiles naturales y edificados de la localidad.",
        "project_image"                 => "images/projects/project6_foz_lugo.jpg",
        "project_image_2"                 => "images/projects/project6_foz_lugo_01.jpg",
        "project_image_2_opt"                 => "images/projects/project6_foz_lugo_01_opt.jpg",
        "project_serv"                  => "paisaje",
        "project_alt"                   => "Ría de Foz, Lugo",
        "project_hover"                 => "Imagen perteneciente a los fondos de la BNE"
    ),

    "project6" => array(
        "project_title"                 => "Restauración de Jardines",
        "project_description"           => "Restauración del jardín de la Quinta de Santa Bárbara",
        "project_addinfo_location"      => "Gijón",
        "project_addinfo_text"          => "Restauración del recreo, las piezas de mosaicocultura y las circulaciones interiores del jardín de la Quinta de Santa Bárbara, en Gijón. Después de una primera fase en la que se abordó la realización del programa global de mantenimiento y la restauración de la entrada posterior del jardín, se procedió a una segunda etapa en la que se revitalizó cada área del mismo. Todas las actuaciones específicas fueron encaminadas a incrementar la variedad y la riqueza del espacio mediante la utilización de plantas vivaces de flor.",
        "project_image"                 => "images/projects/project7_quinta_sb.jpg",
        "project_image_2"                 => "images/projects/project7_quinta_sb_01.jpg",
        "project_image_2_opt"                 => "images/projects/project7_quinta_sb_01_opt.jpg",
        "project_serv"                  => "paisajismo",
        "project_alt"                   => "Quinta Santa Bárbara, Parroquia de Santurio, Asturias",
        "project_hover"                 => "Quinta Santa Bárbara, Parroquia de Santurio, Asturias"
    ),

    "project7" => array(
        "project_title"                 => "Paisajismo",
        "project_description"           => "Inventario de arbolado en cuartel siglo XIX",
        "project_addinfo_location"      => "Sevilla",
        "project_addinfo_text"          => "Inventario de los 1.325 pies arbóreos existentes en el antiguo regimiento de artillería Daoiz y Velarde de Sevilla, originario del siglo XIX, cuyas instalaciones dejaron de funcionar en 1992. Producto del análisis botánico y estructural realizado, se identificaron aquellos árboles que debían mantener su ubicación original; los que, en su defecto, podían ser trasplantados y aquellos que, por el contrario, debían ser talados dado al mal estado que presentaban. En el desarrollo de este proyecto se ha llevado a cabo mediante la utilización de sistemas de información geográfica.",
        "project_image"                 => "images/projects/project8_arbolado_cuartel.jpg",
        "project_image_2"                 => "images/projects/project8_arbolado_cuartel_01.jpg",
        "project_image_2_opt"                 => "images/projects/project8_arbolado_cuartel_01_opt.jpg",
        "project_serv"                  => "paisajismo",
        "project_alt"                   => "Naranjo, Sevilla",
        "project_hover"                 => "Inventario de arbolado, Sevilla"
    ),

    "project8" => array(
        "project_title"                 => "Estudio Energético",
        "project_description"           => "Cálculo del potencial solar fotovoltaico",
        "project_addinfo_location"      => "Vitoria - Gastéiz",
        "project_addinfo_text"          => "Colaboramos con la UPM en la segunda fase de este proyecto de investigación, desarrollando la metodología de trabajo necesaria para calcular el potencial solar fotovoltaico a escala urbana mediante la utilización de modelos digitales de elevaciones (MDE)  realizados a partir de datos LIDAR aportados por Comunidad Autónoma del País Vasco.",
        "project_image"                 => "images/projects/project9_potencia_solar.jpg",
        "project_image_2"                 => "images/projects/project9_potencia_solar_01.jpg",
        "project_image_2_opt"                 => "images/projects/project9_potencia_solar_01_opt.jpg",
        "project_serv"                  => "mma",
        "project_alt"                   => "Potencial solar fotovoltaico en Vitoria – Gastéiz",
        "project_hover"                 => "Potencial solar fotovoltaico en Vitoria – Gastéiz"
    ),

    "project9" => array(
        "project_title"                 => "Creación de Parques",
        "project_description"           => "Parque lineal  en la Línea de la Concepción",
        "project_addinfo_location"      => "Cádiz",
        "project_addinfo_text"          => "Integración ecológica y paisajística del parque lineal perteneciente a la unidad urbanística integrada “Torrenueva 14 C -03” (27 ha) localizado al este de la Línea de la Concepción frente al Mar Mediterráneo. Producto del estudio ecológico y paisajístico realizado,  se modificó  la selección de especies vegetales realizadas previamente por los diseñadores del parque, para integrar el espinar de jérguenes preexistente (hábitat de interés prioritario). Se contemporizó la preservación de las vistas panorámicas del mar, enmarcadas al sur por el Peñón de Gibraltar y al norte por una torre vigía del s.XVIII, con la disposición, sólo en los espacios estanciales, de barreras cortavientos vegetales capaces de filtrar los vientos dominantes también procedentes del este y de esta forma mejorar el confort climático de los usuarios del parque.",
        "project_image"                 => "images/projects/project10_parque_lineal.jpg",
        "project_image_2"                 => "images/projects/project10_parque_lineal_01.jpg",
        "project_image_2_opt"                 => "images/projects/project10_parque_lineal_01_opt.jpg",
        "project_serv"                  => "paisajismo",
        "project_alt"                   => "Parque lineal frente al mediterráneo, Cádiz",
        "project_hover"                 => "Parque lineal frente al mediterráneo, Cádiz"
    )
);
?>