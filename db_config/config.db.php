<?php

    $local_environment = true;

    if ($local_environment)   {

        /*
         * DATABASE CONFIGURATION: Localhost
         */
        $username = "root";
        $password = "toor"; // In linux password = "toor", in windows "toor1234"
        $host = "localhost";
        $dbname = "nahmias_db";

    } else {

        /*
         * DATABASE CONFIGURATION: Server one.com
         */
        $username = "";
        $password = "";
        $host = "";
        $dbname = "";

    }
    

    /*
     * Set characters codifications for the PDO connection.
     */
    $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_PERSISTENT => true);
     
    try 
    { 
        $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
    } 
    catch(PDOException $ex) 
    { 
        die("Failed to connect to the database: " . $ex->getMessage()); 
    } 
     
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
     
    // UNDO Magic Quotes 
    if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) 
    { 
        function undo_magic_quotes_gpc(&$array) 
        { 
            foreach($array as &$value) 
            { 
                if(is_array($value)) 
                { 
                    undo_magic_quotes_gpc($value); 
                } 
                else 
                { 
                    $value = stripslashes($value); 
                } 
            } 
        } 
     
        undo_magic_quotes_gpc($_POST); 
        undo_magic_quotes_gpc($_GET); 
        undo_magic_quotes_gpc($_COOKIE); 
    }

    header('Content-Type: text/html; charset=utf-8');


?>
